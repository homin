json = require 'dkjson'

local function get_json(id)
	--local x=string.format("[[ -s %s ]] || ~/youtube-dl -j -f best https://www.youtube.com/watch?v=%s > %s ; [[ -s %s ]] || ! :",id,id,id,id)
	--repeat print(x) until os.execute(x) == 0

	local s = io.open(id):read('*a')
	local j = json.decode(s)
	return j
end

local function print_dl_curl(id,fmt)
	local j = get_json(id)
	local f = j.formats
	local fid
	--fmt = fmt or 22

	if not fmt then
		for _,fi in ipairs(f) do
			--print(string.format("%s : %sx%s   vcodec:%s   acodec:%s   ext:%s   size:%s",fi.format_id,tostring(fi.width or 'N/A'),tostring(fi.height or 'N/A'),fi.vcodec or 'N/A',fi.acodec or 'N/A',fi.ext,tostring(fi.filesize or 'N/A')))
			print(string.format("%s : %sx%s   vcodec:%s   acodec:%s   ext:%s   size:%s",fi.format_id,tostring(fi.width or 'N/A'),tostring(fi.height or 'N/A'),fi.vcodec or 'N/A',fi.acodec or 'N/A',fi.ext,type(fi.filesize) == 'number' and tostring(fi.filesize) or 'NA'))
		end
		return
	end
	
	for _,fi in ipairs(f) do
		if tonumber(fi.format_id) == tonumber(fmt) then fid = fi break end
	end

	if fid then
		local f
		if fid.vcodec == "none" then f = "fmt"..fmt..':'..fid.acodec
		else
			f = {"fmt",fmt,':',fid.width,'x',fid.height} f = table.concat(f)
		end
		local t,T = os.getenv('YT_ALT'),j.fulltitle:gsub('"',"'"):gsub('/','%%')
		if not t then t=(#T>180) and j.id or T end
		t = {j.upload_date,'-',j.id,#t > 0 and ' ' or '',t,'-',f,'-'}
		t = table.concat(t)
		local plo = os.getenv('YT_PLO')
		if plo then t = string.format('%03d-',tonumber(plo)) .. t end
		local size
		if type(fid.filesize) == 'number' then size = tostring(fid.filesize) else size = '${SIZE}' end
		t = t..size..'.'..fid.ext
		return string.format('(\nTITLE="%s";\nDIR="%s";\nURL="%s";\nALT="%s";\ncurlit temp "$DIR" "$URL" "$ALT"\n)',T,os.getenv("DIR") or '.',fid.url,t)
	else

	end
end

function min_size(id)
	local j = get_json(id)
	local f = j.formats
	local t = {}
	for _,i in ipairs(f) do
		t[i.format_id] = i
	end
	local id,size
	for _,i in ipairs(arg) do
		local r = t[i]
		if r and type(r.filesize) == 'number' then
			local s = tonumber(r.filesize)
			if not size or s < size then size = s id = i end
		end
	end
	if id then io.write(id,' ',size) end
end

function p(id,fmt)
	--fetch_json(id)
	return print_dl_curl(id,fmt)
end

function cor(src,dst)
	local s = io.open(src)
	local d = io.open(dst,'w')
	
	repeat
		local a = s:read(4096)
		if not a then break end
		d:write(a)
	until nil
	
	s:close() d:close()
end
