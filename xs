__def__() {
	local self=__def__
	case $1 in
		gitit)
			$self shit pathit
			gitit() {
				local self=gitit cmd=$1 ; shift
				case $cmd in
					_each_dir)
						local p=${1} x=${2} d D
						shift 2
						for D in ${@:-.} ; do
							for d in $(find ${D} -name "${p}" -type d) ; do
								echo "> ${d}"
								exec echo ${x}
								echo "< ${d}" ; echo
							done
						done
					;;
					gc)
						local d D
						for D in ${@:-.} ; do
							for d in $(find ${D} -name "*.git" -type d) ; do
								echo ${d}
								until GIT_DIR=${d} git gc ; do sync ; done
							echo
							done
						done
					;;
					svnfetch)
						local i
						for i in $(seq $1 $2) ; do until git svn fetch -r ${i} ; do : ; done ; done
					;;
					svninit)
						local repo=${1} dir=${2}.gitsvn
						shift 2
						mkdir ${dir}
						GIT_DIR=${dir} git init --bare
						GIT_DIR=${dir} git svn init "${repo}" ${@:--s}
					;;
					lr)
						local r=${1:?url} p
						p=${r##*/} ; p=${p%.git}
						git ls-remote $r 'refs/heads/*' 'refs/tags/*' | tee lr.$p
					;;
					dup)
						local d c n
						[ $# = 1 ] && { n=$(basename $1) ; n=${n%.git} ; } || n=$2
						git init "${n}"
						c="$(pwd)" ; cd "${1}" ; d="$(pwd)" ; cd "${c}"
						rm -r "${n}"/.git/{objects,refs}
						ln -s "${d}"/{objects,packed-refs} "${n}"/.git || :
						cp -a "${d}"/refs "${n}"/.git
					;;
					tar)
						local d=$(realpath ${GIT_DIR:-.})
						local v b
						b=${1:-master}
						v=$2
						[ "$PREFIX" ] && d="$PREFIX" || {
							d=${d##*/}
							d=${d%.git}
						}
						[ -z "$v" ] && {
							v=$(git describe --tags $b 2>/dev/null || git show --format=format:%h -s $b)
							v=${v#$d-}
						}
						git archive --format=tar --prefix="$d-$v/" $b
					;;
					refhead)
						local h
						h=${1:?ref}
						[ "$h" = "${h#refs/}" ] && h=refs/heads/$h
						#[ -n "$2" ] && h=refs/$2${h#refs}
						echo $h
					;;
					lsref)
						git ls-remote ${GIT_DIR:-.} ${@:-'refs/*'} | awk '{print $2}' | sed '/\^{}$/d'
					;;
					check_shallow)
						local shallow="${GIT_DIR:+$GIT_DIR/}shallow"
						local r=0
						[ -f "$shallow" ] || return
						$self check_branch_completeness $(cat "$shallow") > "$shallow.grafted"
						rm "$shallow"
						[ -s "$shallow.grafted" ] && { mv "$shallow.grafted" "$shallow" ; r=1 ; } || rm "$shallow.grafted"
						return $r
					;;
					check_branch_completeness)
						local b
						local shallow="${GIT_DIR:+$GIT_DIR/}shallow"
						[ -f "$shallow" ] && mv "$shallow" "${shallow}~"
						for b in ${@:-$($self lsref "refs/*")} ; do
							git log --format=%h $b &>/dev/null || echo $b
						done
						[ -f "${shallow}~" ] && mv "${shallow}~" "$shallow"
					;;
					fetch)
						local u="${1:?url}"
						local r=${2:?ref} l d
						r=$($self refhead $r)
						[ -n "$GITIT_NAMESPACE" ] && l=refs/$GITIT_NAMESPACE${r#refs} || l=$r
						if [ "$(git ls-remote . $l)" ] ; then
							d="--negotiation-tip=$l"
						else
							d="--depth ${GITIT_DEPTH:-1}"
						fi
						until
							echo git fetch -v $d "$u" "${GITIT_FORCED_FETCH++}$r:$l"
							git fetch -v $d "$u" "${GITIT_FORCED_FETCH++}$r:$l"
						do : ; done
					;;
					unshallow)
						local u=${1:?url} h r t i l
						shift
						local shallow="${GIT_DIR:+$GIT_DIR/}shallow"
						set -o pipefail
						for h in $@ ; do
							r=$($self refhead $h)
							[ -n "$GITIT_NAMESPACE" ] && {
								l=refs/$GITIT_NAMESPACE${r#refs}
								$self rmref 'refs/tags/*'
								git push ${GIT_DIR:-.} "refs/$GITIT_NAMESPACE/tags/*:refs/tags/*"
							} || l=$r
							t=$(git ls-remote . $l)
							#read -p "t: $t"
							[ "$t" ] || $self fetch "$u" "$h"
							i=0
							while [ -e "$shallow" ] ; do
								i=$((i+1))
								$self check_shallow && break
								cat "$shallow"
								[ "$DEBUG" ] && {
									echo git fetch -v --deepen ${GITIT_DEEPEN:-300} --negotiation-tip=$l "$u" "${GITIT_FORCED_FETCH++}$r:$l"
									read
								}
								git fetch -v --deepen ${GITIT_DEEPEN:-300} --negotiation-tip=$l "$u" "${GITIT_FORCED_FETCH++}$r:$l"
								[ "$DEBUG" ] && read -p "loop $i"
							done
							[ -n "$GITIT_NAMESPACE" ] && {
								git push ${GIT_DIR:-.} "refs/tags/*:refs/$GITIT_NAMESPACE/tags/*"
								$self rmref 'refs/tags/*'
							}
						done
					;;
					lsns)
						$self lsref 'refs/*/heads/*' | sed 's|refs/\(.*\)/heads/.*|\1|' | uniq
					;;
					push_namespace)
						git push ${1:?repo} "${GITIT_FORCED_FETCH++}refs/*:refs/${2:?namespace}/*"
					;;
					fetch_namespace)
						git fetch -v ${GITIT_DEPTH+--depth $GITIT_DEPTH} ${1:?repo} "${GITIT_FORCED_FETCH++}refs/${2:?namespace}/*:refs/*"
					;;
					fetch_namespace_heads)
						local u=${1:?repo}
						local n=${2:?namespace}
						shift 2
						local s
						if [ -n "$1" ] ; then
							local i
							for i in $@ ; do s="$s ${GITIT_FORCED_FETCH++}refs/$n/heads/$i:refs/heads/$i" ; done
						else s="${GITIT_FORCED_FETCH++}refs/$n/heads/*:refs/heads/*"
						fi
						#echo $@ ; echo $u ; echo $n ; echo $s ; read -p '<>'
						(GIT_DIR=$u git push $u "refs/$n/tags/*:refs/tags/*")
						git fetch -v ${GITIT_DEPTH+--depth $GITIT_DEPTH} $u $s
						(GIT_DIR=$u $self rmref "refs/tags/*")
					;;
					fetch_namespace_tags)
						git fetch -v ${GITIT_DEPTH+--depth $GITIT_DEPTH} ${1:?repo} "${GITIT_FORCED_FETCH++}refs/${2:?namespace}/tags/*:refs/tags/*"
					;;
					fetch_namespace)
						git fetch -v ${GITIT_DEPTH+--depth $GITIT_DEPTH} ${1:?repo} "+refs/${2:?namespace}/heads/*:refs/heads/*"
					;;
					sync_branch)
						local b u=${1:?url} s h
						shift
						for b in ${@:-$($self lsref "refs/heads/*")} ; do s=$($self refhead $b) ; h="$h ${GITIT_FORCED_FETCH++}$s:$s" ; done
						git fetch -v "$u" $h
					;;
					rmref)
						: ${1:?ref ...}
						local r
						for r in $($self lsref $@) ; do git push ${GIT_DIR:-.} :$r ; done
					;;
					ref_acp)
						: ${GITIT_BASE:?} ${GITIT_PUSH_REPO:?}
						local d=${1:-$(pwd)}
						local b=$(realpath $GITIT_BASE)
						local p=$(pathit tail "$GITIT_BASE" "$d")
						#read -p "$p"
						[ -z "$p" ] && return 1
						shift
						(
							cd "$d"
							git add .
							git commit -m "${@:-.}"
							git push $GITIT_PUSH_REPO "${GITIT_FORCED_PUSH++}HEAD:refs/$p"
							[ -n "$GITIT_LOCAL_REPO" ] && git push $GITIT_LOCAL_REPO "HEAD:refs/$p"
						)
					;;
					ref_init)
						: ${GITIT_BASE:?} ${GITIT_PULL_REPO:?}
						local b=${1:?ref}
						b=$($self refhead $b)
						local p=$GITIT_BASE/${b#refs/}
						git init "$p"
						(	cd "$p"
							git fetch -v ${GITIT_DEPTH:---depth 1} ${GITIT_DEPTH:+--depth $GITIT_DEPTH} "$GITIT_PULL_REPO" $b:$b- &&
							{
								git checkout $b-
								git push . HEAD:refs/heads/master
								git checkout master
								git push . :$b-
							}
						)
					;;
					ref_deepen)
						: ${GITIT_BASE:?} ${GITIT_PULL_REPO:?}
						local d=${1:-$(pwd)}
						local b=$(realpath $GITIT_BASE)
						local p=$(pathit tail "$GITIT_BASE" "$d")
						echo $p
						shift
						git -C "$d" branch m
						git -C "$d" fetch -v --deepen ${1:-1} "$GITIT_PULL_REPO" "refs/$p:m"
					;;
					ref_pull)
						local r="${1:?repo}"
						local d="${2:?dir}"
						git init "$d"
						( cd "$d" ; git fetch --depth 1 "$u" refs/${d#*/}:tip && git checkout tip )
					;;
					fetch_regular_to_combo)
						:
					;;
					fetch_combo_to_regular)
						:
					;;
					github)
						: ${1?dir}${2?url}
						(
						ns=${2#https://github.com/}
						cd "$1"
						GITIT_NAMESPACE="$ns" gitit unshallow "$2" ${3:-master}
						)
					;;
					*)
						echo "${self} dup <srcdir>"
					;;
				esac
			}
		;;
		alpineit)
			$self apkit
			alpineit() {
				local S=alpineit R=$APK_HOME
				local c=$1
				[ $# -gt 0 ] && shift
				case $c in
					setup)
						: ${1:?dir}
						: ${APK_HOME:=$(realpath "$1")}
						: ${APK_REPO_ALPINE:="main community"}
						: ${APK_SERVER_ALPINE:="http://dl-cdn.alpinelinux.org/alpine"}
						: ${APK_VERSION_ALPINE:=v3.13}
						: ${APK_LOCAL_ALPINE:="$APK_HOME/dist/alpine"}
					;;
					gen-db)
						distroit gen-db "$1" "dist/*/*/*/*/*"
						return
					;;
					gen-repo-path)
						local r
						if [ $# = 0 ] ; then
							for r in $APK_REPO_ALPINE ; do echo $APK_VERSION_ALPINE/$r ; done
						else
							for r in $@ ; do
								#expr " $APK_REPO_ALPINE " : ".* $r " >/dev/null && echo "$APK_VERSION_ALPINE/$r"
								echo "$APK_VERSION_ALPINE/$r"
							done
						fi
						return
					;;
					gen-dl-hash)
						local r
						for r in $($S gen-repo-path) ; do
							echo "${APK_SERVER_ALPINE#*://}/$r/$APK_ARCH=$APK_LOCAL_ALPINE/$r/$APK_ARCH"
						done
						return
					;;
					download-apk-static|extract-apk-static)
						set -- alpine main
					;;
					init)
						$S update-index
						$S download-apk-static
						$S extract-apk-static
						
						#$S simfetch alpine-base
						$S simfetch busybox alpine-baselayout apk-tools alpine-keys
						sudo -k ; sudo -v
						
						#echo sudo $APK_APP_STATIC $($S repo-opt) --allow-untrusted --root "$APK_SYSROOT" --initdb add ${@:-alpine-base}
						#sudo $APK_APP_STATIC $($S repo-opt) --allow-untrusted --root "$APK_SYSROOT" --initdb add ${@:-alpine-base}

						echo sudo $($S echo-cmd add) --initdb ${@:-alpine-base} # alpine-baselayout apk-tools alpine-keys
						sudo $($S echo-cmd add) --initdb ${@:-alpine-base}
					;;
				esac
				local APKIT=$S
				apkit $c "$@"
			}
		;;
		apkit)
			$self curlit distroit fsit sudoit shit strit
			apkit() {
				local self=apkit SH=${APKIT:-apkit} H=$APK_HOME
				local cmd=$1 ; shift 2>/dev/null
				
				case ${cmd} in
					setup)
						: ${1:?APK_HOME}
						local D d r p
						d=$(shit lvpre APK_REPO_)
						APK_DISTS=$(echo $d | sed 's/APK_REPO_//g')
						: ${APK_HOME:=$(realpath "$1")}
						: ${APK_ARCH:=$($self print-arch)}
						
						for D in $APK_DISTS ; do
							d=$(strit lower $D)
							p=$(shit ive APK_LOCAL_$D)
							for r in $(${d}it gen-repo-path) ; do
								mkdir -pv "$p/$r/$APK_ARCH"
							done
						done
						return
						;;
					state)
						local s v
						v=$(shit lvpre APK_)
						[ "$v" ] || echo "APK_ARCH APK_DISTS APK_HOME APK_REPO_ALPINE APK_SERVER_ALPINE APK_VERSION_ALPINE" &&
						for s in $v ; do
							echo "$s=$(shit ive $s)"
						done
					;;
					unstate)
						unset $(shit lvpre APK_)
					;;
					print-arch)
						if [ "$APK_ARCH" ] ; then echo "$APK_ARCH"
						elif [ -x "$APK_APP_STATIC" ] ; then $APK_APP_STATIC --print-arch
						else uname -m
						fi
					;;
					list-dist)
						local d
						for d in ${APK_DISTS_ORDER:-$APK_DISTS} ; do echo $(strit lower $d) ; done
					;;
					list-dist-repo)
						: ${1:?dist}
						local d=APK_REPO_$(strit upper $1) r
						for r in $(shit ive $d); do echo $r ; done
					;;
					update-index)
						local a d D l s dist
						for dist in $APK_DISTS ; do
							d=$(strit lower $dist)
							D=$(strit upper $dist)
							s=$(shit ive APK_SERVER_$D)
							r=$(${d}it gen-repo-path)
							for l in $r ; do
								echo curlit sync "$H/dist/$d/$l/$APK_ARCH" "$s/$l/$APK_ARCH/APKINDEX.tar.gz"
								(CURLIT_SYNC_DB_IGNORE=1 curlit sync "$H/dist/$d/$l/$APK_ARCH" "$s/$l/$APK_ARCH/APKINDEX.tar.gz")
							done
						done
					;;
					download-apk-static) # dist repo
						: ${1:?dist} ${2:?repo}
						local d=$1 p r=$2 s v
						p=$(${d}it gen-repo-path $r)/$APK_ARCH
						s=$(shit ive APK_SERVER_$(strit upper $d))
						v=$(bsdtar -x -O -f "$H/dist/$d/$p/APKINDEX.tar.gz" APKINDEX | sed -n '/P:apk-tools-static/,/^$/s/^V://p')
						u="$s/$p/apk-tools-static-$v.apk"
						echo curlit sync "$H/dist/$d/$p" "$u"
						curlit sync "$H/dist/$d/$p" "$u"
					;;
					extract-apk-static) # dist repo
						: ${1:?dist} ${2:?repo}
						local d=$1 p r=$2 v
						p=$(${d}it gen-repo-path $r)/$APK_ARCH
						if [ -f "$H/dist/$d/$p/APKINDEX.tar.gz" ] ; then
							v=$(bsdtar -x -O -f "${H}/dist/${d}/${p}/APKINDEX.tar.gz" APKINDEX | sed -n '/P:apk-tools-static/,/^$/s/^V://p')
							f="$H/dist/$d/$p/apk-tools-static-$v.apk"
						else
							f="$H/dist/$d/$p"/apk-tools-static-*.apk
						fi
						APK_SYSROOT="$H/sysroot/$d/$(shit ive APK_VERSION_$(strit upper $d))/$APK_ARCH"
						mkdir -p "$APK_SYSROOT" && ( cd "$APK_SYSROOT" && bsdtar -x -f "$f" sbin/apk.static )
						APK_APP_STATIC="$APK_SYSROOT/sbin/apk.static"
						$self cmd add --initdb
					;;
					repo-opt)
						local d r 
						if [ "$1" ] ; then
							local p
							while [ "$1" ] ; do
								d=${1%/*}
								r=${1#*/}
								p=$(${d}it gen-repo-path $r 2>/dev/null)
								[ "$p" ] && echo "-X $H/dist/$d/$p"
								shift
							done
						else
							for d in $($self list-dist) ; do
								for r in $(${d}it gen-repo-path) ; do
									[ "$r" ] && echo "-X $H/dist/$d/$r"
								done
							done
						fi
					;;
					cmdrepo|echo-cmdrepo)
						: ${2:?cmd} ${APK_APP_STATIC:?}
						local repos=$1
						shift
						[ "$cmd" = "echo-cmdrepo" ] && cmd=echo || cmd=
						$cmd $APK_APP_STATIC $($self repo-opt $repos) --allow-untrusted -p "$APK_SYSROOT" "$@"
					;;
					cmd|echo-cmd)
						: ${1:?cmd} ${APK_APP_STATIC:?}
						#$APK_APP_STATIC $($self repo-opt) --allow-untrusted -p "$APK_SYSROOT" "$@"
						[ "$cmd" = "echo-cmd" ] && cmd=echo- || cmd=
						$self ${cmd}cmdrepo '' "$@"
					;;
					search) # <pkg>
						$self cmd search "$@"
					;;
					rep_info)
						local r
						while read -p '>>> ' r ; do [ "$r" ] && $self cmd info -a $r ; done
					;;
					simfetch) # <pkg ...>
						: ${1:?pkg ...}
						$self cmd fetch --simulate -R "$@" | sed 's|^Downloading ||' | sort -u
					;;
					which-repo) # <pkg>
						: ${1:?pkg}
						local d r o s D
						for d in $($self list-dist) ; do
							D=$(strit upper $d)
							for r in $(shit ive APK_REPO_$D) ; do
								o=$($self cmdrepo $d/$r search -x $1)
								if [ "$o" ] ; then
									s=$(shit ive APK_SERVER_$D)
									echo "$s/$(${d}it gen-repo-path $r)/$APK_ARCH"
									break 2
								fi
							done
						done
					;;
					pv)
						local p l r
						for p in $(cat "$1") ; do
							l=${p%-*-r*}
							r=${p#$l-}
							echo "$l $r"
						done
					;;
					gen-dl)
						local p l
						for p in $($self simfetch "$@") ; do
							l=$($self which-repo ${p%-*-r*})
							echo "$l/$p.apk"
						done
					;;
					dlt) # <file>
						: ${1:?file}
						local a d
						for d in $($self list-dist) ; do
							read -p "download distro ${d}? " a
							[ "$a" = y ] &&
							echo curlit table "$1" $(${d}it gen-dl-hash)
						done
					;;
					check)
						distroit check "$1" "$(shift ; [ -z "$1" ] && echo 'cd $''d ; ls dist/*/*/*/*/*' || echo "$@")"
					;;
					whatnew)
						distroit whatnew "$1" "$2" 'dist/*/*/*/'"$($self print-arch)"
					;;
					init)
						local d r
						sudo mkdir -p "$APK_SYSROOT/dist"
						sudo /bin/sh -c "rm $APK_SYSROOT/etc/apk/repositories 2>/dev/null"
						for d in $($self list-dist) ; do
							for r in $(${d}it gen-repo-path) ; do 
								sudo /bin/sh -c "echo /dist/$d/$r >> $APK_SYSROOT/etc/apk/repositories"
							done
						done
					;;
					chroot)
						$self mountsys "$1"
						distroit chroot "${1:-$APK_SYSROOT}}"
					;;
					mountsys)
						local d=${1:-$APK_SYSROOT}
						fsit mpfm "$d"
						fsit mb "$H/dist" "$d/dist"
					;;
					chroot_umount)
						local s=$(sudoit init)
						$self mountsys "$1"
						$self chroot "$1"
						sudoit refresh "$s"
						$self umountsys "$1"
					;;
					umountsys)
						local d=${1:-$APK_SYSROOT}
						fsit ua "$d"
					;;
					*)
						echo "$SH setup <dir>"
						echo "$SH state | unstate | print-arch"
						echo "$SH update-index"
						echo "$SH init [ alpine-baselayout apk-tools alpine-keys ]"
						echo "$SH download-apk-static | extract-apk-static <dist> <repo>"
						echo "$SH chroot"
						echo "$SH search | simfetch | which-repo <pkg ...>"
						echo "$SH gen-dl <pkg ...>"
						echo "$SH dlt <file>"
					;;
				esac
			}
		;;
		luautil)
			luautil() {
				local S=luautil c=$1 e=0
				[[ $# -gt 0 ]] && shift
				case $c in
					cat) # <file> <block_size> <offset> [block_count]
						local s="(function(f,b,o,n) f = assert(io.open(f)) b = assert(tonumber(b)) o = assert(tonumber(o)) n = n and assert(tonumber(n)) assert(f:seek('set',o)) local c = 0 repeat local d = f:read(b) if d then io.write(d) io.flush() else break end c = c + 1 until n and c >= n f:close() end)(table.unpack(arg))"
						e=1
					;;
					http-headers) # <sep> <file> <header ...>
						local s="(function(s,h,...) h=io.open(h) local t={} repeat local l,a,v=h:read('*l') if not l then break end a,v=l:match '^([^:]-): (.-)\r$' if a then t[a:lower()]=v end until nil h:close() local r,f,i={},{...} for i=1,#f do local s=string.lower(f[i]) r[#r+1]=t[s] or '' end io.write(table.concat(r,s)) io.flush() end)(table.unpack(arg))"
						e=1
					;;
				esac
				(( e )) && lua5.3 -e "${s}" /dev/null "$@"
			}
		;;
		curlit)
			$self luautil
			curlit() {
				local self=curlit
				local cmd=$1
				[ $# -gt 0 ] && shift
				case $cmd in
					temp)
						local min_size block_size save_dir url base_file base_size temp_size start block new_base_size base_size_align r
						save_dir=${1:?}
						url=${2:?}
						base_file=${3:-${2##*/}}
						[ ! -d "${save_dir}" ] && return 1
						min_size=${CURLIT_TEMP_MS:-$(( 300*1024*1024 ))}
						block_size=${CURLIT_TEMP_BS:-$(( 4*1024 ))}
						[ ${min_size} -lt ${block_size} ] && return 2
						[ -f "${save_dir}/${base_file}" ] && base_size=$(stat -L -c %s -- "${save_dir}/${base_file}") || base_size=0
						base_size_align=$(( ${base_size} % ${block_size} ))
						until
						temp_size=$(stat -L -c %s -- "${base_file}.${base_size}" 2>/dev/null) || temp_size=0
						if [ ${temp_size} -gt ${min_size} ] ; then
							local n=0
							if [ ${base_size_align} -gt 0 ] ; then
								n=$(( ${block_size}-${base_size_align} ))
								luautil cat "${base_file}.${base_size}" $n 0 1 >> "${save_dir}/${base_file}" || return 3
								base_size_align=0
							fi
							block=$(( (${temp_size}-${n}) / ${block_size} ))
							if [ $block -gt 0 ] ; then
								if [ $n -gt 0 ] ; then
									luautil cat "${base_file}.${base_size}" $block_size $n $block >> "${save_dir}/${base_file}" || return 4
								else
									luautil cat "${base_file}.${base_size}" $block_size 0 $block >> "${save_dir}/${base_file}" || return 5
								fi
							fi
							n=$(( ${n} + (${block}*${block_size}) ))
							new_base_size=$(( ${base_size} + ${n} ))
							luautil cat "${base_file}.${base_size}" $block_size $n > "${base_file}.${new_base_size}" && rm -- "${base_file}.${base_size}" || return 6
							base_size=$new_base_size
							temp_size=$(( ${temp_size} - ${n} ))
							if [ -n "${DLI_HOOK_SYNC}" ] ; then echo "DLI_HOOK_SYNC: $DLI_HOOK_SYNC" ; $DLI_HOOK_SYNC ; fi
							[ -x dlihook ] && ./dlihook
						fi
						start=$(( ${base_size} + ${temp_size} ))
						echo "$base_file" 1>&2
						local pre=$(date '+%s');
						curl -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Falkon/3.1.0 Chrome/69.0.3497.128 Safari/537.36' -L -k -N -Y 100 -y 13 -C ${start} ${CURLIT_OPTS} -D "${base_file}.h" "${url}" >> "${base_file}.${base_size}"
						r=$?
						read -t 1 -p "curl return: $r"
						[ 0 = $r ]
						do
							case $r in
								33)
									break
								;;
								63) # max-filesize
									break
								;;
							esac
							local post=$(date '+%s')
							[ $(( $post - $pre )) -le 3 ] && read -t 3 -p 'too fast, slowing down '
						done
						#[ -s "${base_file}.${base_size}" ] || { rm -- "${base_file}.${base_size}" ; return 7 ; }
						local cl fs lu
						local status=$(sed -n -- '/^HTTP/p' "${base_file}.h" | tail -1)
						if [ -n "${status}" ] ; then
							case ${status} in
								HTTP/*\ 20?\ *)
									fs=$(stat -L -c %s -- "${base_file}.${base_size}")
									case ${status} in
										HTTP/*\ 200\ *)
											cl=$(luautil http-headers "" "${base_file}.h" "Content-Length") ;;
										*)
											cl=$(luautil http-headers "" "${base_file}.h" "Content-Range") ; cl=${cl#*/}
											fs=$(( ${fs} + ${base_size} ))
										;;
									esac
								;;
								*)
									return 10
								;;
							esac
							lu=$(luautil http-headers "" "${base_file}.h" "Last-Modified")
						fi
						
						echo "cl:$cl fs:$fs" 1>&2
						if [ $r = 0 -a "$cl" = "$fs" ] ; then
							if [ $base_size = 0 ] ; then
								mv -- "${base_file}.0" "${save_dir}/${base_file}" || return 8
							else
								echo "cat \"${base_file}.${base_size}\" >> \"${save_dir}/${base_file}\"" 1>&2
								echo "rm -- \"${base_file}.${base_size}\"" 1>&2
								cat -- "${base_file}.${base_size}" >> "${save_dir}/${base_file}" && rm -- "${base_file}.${base_size}" || return 9
							fi
							[ -n "$lu" ] && touch -d "$lu" -- "$save_dir/$base_file"
							echo "$save_dir/$base_file $fs $(date ${lu:+-d "$lu"} '+%s')"
						fi
					;;
					attr)
						local u=${1:?} sl=${2:-0} dl=${3:-0} sr dr r f=$(mktemp XXXXX) s c=0
						until curl -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Falkon/3.1.0 Chrome/69.0.3497.128 Safari/537.36' -kILs --connect-timeout 30 "$u" > "${f}" ; do
							c=$(( c + 1))
							[ "$CURLIT_ATTR_RETRY" ] && [ $c -ge $CURLIT_ATTR_RETRY ] && { rm -- "$f" ; return 1 ; }
						done
						s=$(luautil http-headers "|" "${f}" "Content-Length" "Last-Modified")
						IFS="|" ; set -- $s ; unset IFS
						sr=$1 ; [ -z "$sr" ] && sr=0
						dr=$2 ; [ -z "$dr" ] && dr=0 || dr=$(date -d "$dr" "+%s")
						if [ "$sl" = "$sr" -a "$dl" = "$dr" ] ; then r=0 ; s='='
						elif [ "$sl" = "$sr" ] ; then [ "$dl" -lt "$dr" ] && { r=2 ; s='T' ; } || { r=3 ; s='t' ; }
						elif [ "$dl" = "$dr" ] ; then [ "$sl" -lt "$sr" ] && { r=4 ; s='S' ; } || { r=5 ; s='s' ; }
						else r=1 ; s='x'
						fi
						[ -e "$f" ] && rm -- "$f"
						echo "$sr $dr $s"
						return $r
					;;
					attrf)
						: ${1:?}
						local u f
						for u in $(cat $1) ; do
							f=${u##*/}
							echo "$u" > curlit.attr
							set -- $(sed -n /${f//./\\.}/p curlit.ok)
							f=$1
							set -- $(curlit attr $u $2 $3)
							[ "$3" != '=' ] && echo $f $@ $u
							done
						;;
					exist-db)
						local d="${1:?dir}" f="${2:?file}" s r
						local p
						[ -n "$NOPATH" ] && p=$f || p="$d/$f"
						#read -p "$p"
						s=$(sed -n -- "\.${p//./\\.}.p" "${CURLIT_EXIST:-curlit.ok}" "${CURLIT_ERROR:-curlit.err}" 2>/dev/null | while read s ; do
							set -- $s
							if [ "${1##*/}" = "$f" ] ; then shift ; echo $* ; break ; fi
						done)
						[ -n "$s" ] && { r=0 ; echo "$s" ; } || r=1
						return $r
					;;
					exist-local)
						local d="${1:?}" f="${2:?}" r
						[ -f "$d/$f" ] &&	{
							r=0
							echo $(stat -L -c "%s %Y" -- "$d/$f")
						} || r=1
						return $r
					;;
					exist)
						local d="${1:?}" f="${2:?}" s
						[ -z "$CURLIT_SYNC_DB_IGNORE" ] && s=$($self exist-db "$d" "$f") || s=$($self exist-local "$d" "$f") && echo $s
					;;
					sync)
						local d="${1:?}" u="${2:?}" f="$3" s r sync
						[ -z "$f" ] && f=${u##*/}
						s=$($self exist "$d" "$f")
						r=$?
						set -- $s
						if [ "$CURLIT_SYNC_REMOTE" = 1 ] ; then
							s=$($self attr "$u" $*)
							r=$?
							set -- $s
							echo ${u##*/} $r $*
							case $r in
								0) return ;;
								2|3)
									read -p "(local = remote) size, sync time? " r
									if [ "$r" = y ] ; then
										[ -f "$d/$f" ] && touch -d @$2 "$d/$f"
										sed -i "/^$f/d" "${CURLIT_EXIST:-curlit.ok}" 2>/dev/null
										echo "$f $1 $2" >> "${CURLIT_EXIST:-curlit.ok}"
									fi
								;;
								4|5) echo "$r (local = remote) date" ; sync=1 ;;
								1) sync=1 ;;
							esac
						else
							if [ $r = 0 ] ; then return
							else sync=1
							fi
						fi
						if [ "$sync" = 1 ] ; then
							echo "$u"
							[ -n "$CURLIT_SYNC_DRYRUN" ] && return
							[ -n "$CURLIT_SYNC_INTERACTIVE" ] && {
								read -p "sync? " r
								[ "$r" != y ] && return 
							}
							[ -f "$d/$f" ] && mv -f -- "$d/$f" "$d/$f".old
							if s=$($self temp "$d" "$u" "$f") ; then
								[ -z "$CURLIT_SYNC_DB_IGNORE" ] && {
									sed -i "/^$f/d" "${CURLIT_EXIST:-curlit.ok}" 2>/dev/null
									echo $s >> "${CURLIT_EXIST:-curlit.ok}"
								}
								rm -- ${f}.h
								[ -f "$d/$f".old ] && rm -- "$d/$f".old
							else
								[ -z "$CURLIT_SYNC_DB_IGNORE" ] && {
									echo $f 0 0 >> "${CURLIT_ERROR:-curlit.err}"
								}
							fi
						fi
					;;
					table)
						local l=${1:?}
						shift
						local t p d u r
						for t in $@ ; do
							p=${t%=*} ; d=${t#*=}
							[ -d "${d}" ] || mkdir -p "$d"
							for u in $(grep -v '^#' $l | grep "$p") ; do
								[ -n "$HOOK" ] && $HOOK "$d" "$u"
								if [ -n "$CURLIT_TABLE_DRYRUN" ] ; then
									echo "${d}/${u##*/}"
								else
									$self sync "$d" "$u"
								fi
								[ -f pause ] && read -p 'pause...'
							done
						done
					;;
					null)
						while : ; do curl -kLN --connect-timeout 10 --limit-rate 40000 -Y 300 -y 5 "$1" > /dev/null ; done
					;;
					*)
						echo "CURLIT_TEMP_MS=$((300*1024*1024)) CURLIT_TEMP_BS=4096 CURLIT_OPTS='--limit-rate 33000'"
						echo "$self temp <dir> <url> [alt]"
						echo "$self attr <url> [size] [time]"
						echo "CURLIT_SYNC_REMOTE=1 CURLIT_SYNC_DRYRUN=1 CURLIT_SYNC_INTERACTIVE=1 CURLIT_SYNC_DB_IGNORE=1"
						echo "$self sync <dir> <url> [alt]"
						echo "CURLIT_TABLE_DRYRUN=1"
						echo "$self table <file> <patern=dir ...>"
						echo "$self null <url>"
					;;
				esac
			}
		;;
		pacmanit)
			$self curlit distroit shit sudoit pathit setit
			set -o pipefail
			pacmanit() {
				local S=pacmanit SH=${PACMANIT:-pacmanit} R=$PACMAN_HOME
				local c=$1
				[ $# -gt 0 ] && shift
				case $c in
					setup)
						: ${1:?}
						local d s
						PACMAN_DISTS=${PACMAN_DISTS_ORDER^^}
						: ${PACMAN_HOME:=$(realpath "$1")}
						: ${PACMAN_ARCH:=$(uname -m)}
						for d in $PACMAN_DISTS ; do
							mkdir -p "$R"/dist/${d,,}/{repo,sync}
							s=PACMAN_SERVER_$d
							echo "Server = ${!s}" > "$R"/dist/${d,,}/mirror-list
						done
						[ "$PACMAN_DB" ] && mkdir -p "$PACMAN_DB"/{gnupg,local,pkg,sync,conf}
						[ -e "$PACMAN_DB/conf/pacman.conf" ] || $S gen-conf
					;;
					state)
						local v l
						l=$(shit lvpre PACMAN_)
						for v in $l ; do echo "$v='$(shit ive $v)'" ; done
					;;
					unstate)
						unset ${!PACMAN_@}
					;;
					gen-repo-list)
						local d r s
						for d in ${PACMAN_DISTS_ORDER:-$PACMAN_DISTS} ; do
							echo "# ${d,,}"
							s=PACMAN_REPO_${d^^}
							for r in ${!s} ; do
								echo "[$r]"
								echo "Include = $R/dist/${d,,}/mirror-list"
							done
							echo
						done
					;;
					gen-pkg)
						expac '%n %N %G' | sed -n '/  $/s/  $//p'
						return
						local p s c
						for p in $(pacman $PACMAN_OPTS -Qeq 2>/dev/null) ; do
							s=$(pacman $PACMAN_OPTS -Qi $p 2>/dev/null | grep -e '^Groups *: None' -e '^Required By *: None')
							case $s in Groups*Required*) c=a ;; Groups*) c=g ;; Required*) c=r ;; '') c=0 ;; esac
							if [ $c = a -o $c = r ] ; then echo "$c $p" ; fi
							#echo "$c $p" $s
						done
					;;
					gen-conf)
						{ cat <<"#PACMAN.CONF#"
#
# /etc/pacman.conf
#
# See the pacman.conf(5) manpage for option and repository directives

#
# GENERAL OPTIONS
#
[options]
# The following paths are commented out with their default values listed.
# If you wish to use different paths, uncomment and update the paths.
#RootDir     = /
DBPath      = @PACMAN_DIR@/
CacheDir    = @PACMAN_DIR@/pkg/
LogFile     = @PACMAN_DIR@/pacman.log
GPGDir      = @PACMAN_DIR@/gnupg/
HookDir     = @PACMAN_DIR@/hooks/
HoldPkg      = pacman glibc
#XferCommand = /usr/bin/curl -C - -f %u > %o
#XferCommand = /usr/bin/wget --passive-ftp -c -O %o %u
#CleanMethod = KeepInstalled
#UseDelta    = 0.7
Architecture = auto

# Pacman won't upgrade packages listed in IgnorePkg and members of IgnoreGroup
#IgnorePkg   =
#IgnoreGroup =

#NoUpgrade   =
#NoExtract   =

# Misc options
#UseSyslog
#Color
#TotalDownload
CheckSpace
#VerbosePkgLists

# By default, pacman accepts packages signed by keys that its local keyring
# trusts (see pacman-key and its man page), as well as unsigned packages.
#SigLevel    = Required DatabaseOptional
SigLevel    = Never
LocalFileSigLevel = Optional
#RemoteFileSigLevel = Required

# NOTE: You must run `pacman-key --init` before first using pacman; the local
# keyring can then be populated with the keys of all official Cromnix
# packagers with `pacman-key --populate archlinux cromnix`.

#
# REPOSITORIES
#   - can be defined here or included from another file
#   - pacman will search repositories in the order defined here
#   - local/custom mirrors can be added here or in separate files
#   - repositories listed first will take precedence when packages
#     have identical names, regardless of version number
#   - URLs will have $repo replaced by the name of the current repo
#   - URLs will have $arch replaced by the name of the architecture
#
# Repository entries are of the format:
#       [repo-name]
#       Server = ServerName
#       Include = IncludePath
#
# The header [repo-name] is crucial - it must be present and
# uncommented to enable the repo.
#

#PACMAN.CONF#
						} | sed "s|@PACMAN_DIR@|$PACMAN_DB|g" > "$PACMAN_DB/conf/pacman.conf"
						$S gen-repo-list >> "$PACMAN_DB/conf/pacman.conf"
					;;
					sync-db)
						local d r u l dist repo
						for d in ${@:-${PACMAN_DISTS_ORDER:-$PACMAN_DISTS}} ; do
							#read -p "sync ${d,,} repo? " r
							: "$r" && {
								dist=${d%/*}
								[ "$dist" = "$d" ] && repo= || repo=${d#*/}
								u=$(shit ive PACMAN_SERVER_${dist^^})
								for r in ${repo:-$(shit ive PACMAN_REPO_${dist^^})} ; do
									l="$(repo=$r arch=$PACMAN_ARCH eval echo $u)/$r.db"
									#echo curlit sync "$R/dist/${d,,}/sync" "$l"
									(CURLIT_SYNC_DB_IGNORE=1 curlit sync "${R}/dist/$dist/sync" "$l")
								done
							}
						done
					;;
					pacman)
						pacman --config "$PACMAN_DB"/conf/pacman.conf "$@"
					;;
					expac)
						expac -S -l ' ' --config "$PACMAN_DB"/conf/pacman.conf "$@"
					;;
					gen-dl)
						: ${1:?}
						#local f="$(realpath "$1").dlt"
						local f="$1.dlt" r
						shift
						pacman --config "$PACMAN_DB"/conf/pacman.conf -Sp $(sed -e '/^#/d;s/^. //' $@) | sort -u > "$f" || return 1
						grep -Fi systemd "$f" 1>&2
						:
					;;
					gen-dl-db)
						distroit gen-db "$1" "dist/*/repo/*/*"
					;;
					gen-dl-hash)
						local d r u l sd sr alias
						for d in ${@} ; do
							sd=${d%/*}
							if [ $sd = $d ] ; then
								sr=PACMAN_REPO_${d^^}
								sr=${!sr}
							else
								sr=${d#*/}
							fi
							u=PACMAN_SERVER_${sd^^}
							u=${!u}
							for r in $sr ; do
								l="$(repo=$r arch=$PACMAN_ARCH eval echo $u)"
								l=${l#*://}
								alias=$(shit ive PACMAN_REPO_${sd^^}_ALIAS)
								echo "$l=dist/${alias:-${sd,,}}/repo/$r"
							done
						done
					;;
					gen_not_required)
						#$S expac '%n %N' | sed -n '/ $/ s| $||p'
						$S expac '%n %N' "$@" | awk '$2 == "" { print $1 }'
					;;
					gen_provide_all)
						$S expac '%r/%n %P' | sed "/ $/d"
					;;
					gen_provide_from_dlt)
						local p
						for p in $($S expac -1 "%P" $($S x_name "$1")) ; do echo $p ; done | sort -u
					;;
					query_pkg_that_provide) # <provide ...>
						local f p l gen
						[ "$PROVIDE_DB" ] && f="$PROVIDE_DB" || {
							f=$(mktemp XXXXX.provide_all)
							$S gen_provide_all > "$f"
							gen=1
						}
						for p in $@ ; do
							echo $p $(for s in $(sed -n -e "/ $p[ =<>]/p ; / $p"'$/p' "$f" | awk '{print $1}') ; do echo ${s#*/} ; done | sort -u)
						done
						[ "$gen" ] && rm "$f"
					;;
					revprov) # <tag>
						: ${1?tag}
						local t="$1"
						[ -f "$t".provide.all ] || $S gen_provide_all > "$t".provide.all
						PROVIDE_DB="$t".provide.all $S query_pkg_that_provide $($S gen_provide_from_dlt "$t".dlt) > "$t".rev_provide.dlt
						sed -e "/^[^ ]\+ [^ ]\+$/d" "$t".rev_provide.dlt > "$t".rev_provide.dlt3
						$S tag_selected_pkg_that_provide "$t" "$t".rev_provide.dlt3 | tee "$t".rev_provide.dlt3.tag
					;;
					dlt)
						: ${1:?}
						local f=$(realpath "$1")
						shift
						(
						cd "$R"/tmp || return 1
						curlit table "$f" $($S gen-dl-hash ${@:-$PACMAN_DISTS})
						)
					;;
					outdate0) # <tag> <dir>
						: ${1?tag}${2?dir}
						local a=$(mktemp XXXXX) b=$(mktemp XXXXX)
						(cd "$2" ; ls dist/*/repo/*/*) > "$a"
						(CURLIT_SYNC_DRYRUN=1 CURLIT_TABLE_DRYRUN=1 $S dlt "$1".dlt) > "$b"
						setit lua_fa-fb "$a" "$b"
						rm -- "$a" "$b"
					;;
					outdate) # <tag> <dir>
						: ${1?tag}${2?dir}
						local o n
						o=$(cd "$2" ; ls dist/*/repo/*/*)
						n=$(CURLIT_SYNC_DRYRUN=1 CURLIT_TABLE_DRYRUN=1 $S dlt "$1".dlt)
						setit x-y o n
					;;
					purge_outdate) # <tag> <dir>
						: ${1?tag}${2?dir}
						local n a
						a=$($S outdate "$1" "$2")
						[ "$a" ] || { echo up to date ; return ; }
						n=$(cd "$2" ; ls outdate*.tzs | sort -n | tail -1)
						if [ "$n" ] ; then
							n=${n#outdate}
							n=${n%.tzs}
							n=$((n+1))
						else n=1
						fi
						(cd "$2" ; tar c $a) | zstd -19 > "$2"/outdate${n}.tzs
						(cd "$2" ; rm $a)
					;;
					check)
						local d="${1:-$PACMAN_HOME}"
						[ -n "$1" ] && shift
						distroit check "$d" "$(if [ -z "$1" ] ; then echo 'ls dist/*/repo/*/*' ; else echo "$@" ; fi)"
					;;
					backup)
						local d="${1:?}"
						shift
						distroit cp "$R" "$d" "$([ -z "$1" ] && echo 'cd $''PACMAN_HOME ; ls dist/*/repo/*/*' || echo "$@")"
						sync
					;;
					whatnew)
						[ -f "$2".dlt ] || $S gen-dl "$2"
						distroit whatnew "$1" "$2".dlt $(shift 2 ; $S gen-dl-hash ${@:-$PACMAN_DISTS})
					;;
					snapshot)
						local f s t
						f=$($S whatnew "$1" "$2")
						s="${f%.new}"
						t=${2%.dlt}
						
						#[ -s "$f" ] || { cat "$t" ; echo ; awk '{print $1}' "$s.cached" ; } > "$t.snap"
						
						shift 2
						[ -s "$f" ] || {
							[ "$*" ] && pathit cat $@ | sed '/^[ \t]*$/d' || echo "###"
							echo
							awk '{print $1}' "$s.cached"
						} > "$t.snap"
					;;
					attr_cached)
						local l p u d f
						cat "$1" | while read l ; do
							p=${l% *}
							d=${p%/*}
							f=${p##*/}
							u=${l#* }
							echo "$p" $(curlit attr "$u" $(curlit exist-db "$d" "$f"))
						done
					;;
					gen-db)
						: ${1:?} ${2:?}
						local repo r db wd=$(pwd) distdir=$(realpath "$1") pl=$(realpath "$2")
						repo=$(distroit pkg-x-path "$pl" | sed 's|/[^/]\+$||' | sort -u)
						for r in $repo ; do
							echo $r
							db=${r##*/}
							( cd "$distdir/$r" ; repo-add "$wd/$db.db.tar.gz" $(distroit pkg-x-path "$pl" | grep -F -- $r | sed "s|$r/||") )
						done
					;;
					mount_cache)
						: ${1:?}
						local m="" l d p r
						p=$(realpath $1)
						for d in $PACMAN_DISTS_ORDER ; do
							r=$(eval echo \$PACMAN_REPO_${d^^})
							[ "$r" ] &&
							for l in $r ; do m="$m:$p/dist/$d/repo/$l" ; done ||
							for l in $(echo $p/dist/$d/repo/*) ; do m="$m:$l" ; done
						done
						m=${m#:}
						p=${2:-"$R/db/pkg"}
						fsit mo overlay "$p" -t overlay -o lowerdir=$m
					;;
					rep_info)
						local l r p a
						if [ "$1" ] ; then
							l=$(echo $($S x_name "$1"))
						fi
						while read -p '>>> ' r ; do
							[ "$r" ] && {
								$S pacman -Si $r || {
									echo group: ; $S pacman -Sg $r ||
									echo search: ; $S pacman -Ssq $r
								}
								a=$($S expac -1 '%N' $r)
								echo require by: ; 
								if [ "$1" ] ; then
									for p in $a ; do
										setit in l $p && echo $p
									done
								else echo $a
								fi
							}
						done
					;;
					query_optdep_1)
						expac -S --readone -l ' ' --config "$PACMAN_DB"/conf/pacman.conf "%n %o" $@
					;;
					query_optdep_2)
						$S query_optdep_1 $@ | sed '/ $/d'
					;;
					query_optdep_3)
						$S query_optdep_1 $@ | sed -e '/ $/d' -e 's| .*$||'
					;;
					query_optdep_dlt)
						local p s n=500 i=0 ni nt
						
						while : ; do
							ni=$(((n*i) + 1))
							nt=$((ni + n -1))
							s=$($S x_name "$1.dlt" | sed -n $ni,$nt"p")
							[ "$s" ] || break
							$S query_optdep_3 $s
							i=$((i+1))
						done
					;;
					x_name_ver)
						local l n v
						for l in $(cat $@) ; do l=${l##*/} ; l=${l%-*} ; n=${l%-*-*} v=${l#$n-} ; echo $n $v ; done
					;;
					x_name)
						: ${1:?}
						$S x_name_ver "$1" | sed 's| .*||'
					;;
					whole) # tag pl-dir
						local opt unq
						$S gen-dl "$1" "$2"/pkg/* "$2"/opt/* || return 1
						opt=$($S query_optdep_dlt "$1")
						#unq=$(for p in $(sed -e '/^#/d' -e 's/ .*//' "$2"/opt/*) ; do echo $p ; done | sort -u)
						unq=$(awk '{print $1}' "$2"/opt/* | sed '/^#/d')
						setit uniq opt
						setit uniq unq
						setit x-y opt unq
					;;
					rewhole) # <tag> <theme dir>
						local po lpo p ign opt r
						[ -f "$2"/opt/all ] && {
							read -p "$2/opt/all exists, delete? " r
							[ "$r" = y ] && rm "$2"/opt/all "$1".subs
							rm "$1".unsolve
						}
						ln -s "$2"/opt/all "$1".opt
						#ign=$(sed 's| .*$||' "$2"/ignore/*)
						ign=$(awk '{print $1}' "$2"/ignore/*)
						setit uniq ign
						#read -p "$ign"
						while : ; do
							lpo=$po
							po=$($S whole "$@")
							r=$?
							echo r: $r
							[ $r = 0 ] || break
							echo "lpo: $(echo $lpo)"
							read -p "po: $(echo $po)"
							[ "$po" -a "$po" != "$lpo" ] || { [ "$po" ] && echo $po > "$1".unsolve ; break ; }
							for p in $po ; do
								SORTED=1 setit in ign $p && continue
								local l= s=
								for opt in $($S expac -1 '%o' $p) ; do
									SORTED=1 setit in ign $opt && {
										#read -p "ignore $opt from $p"
										local s=$(sed -n -e "/^$opt""$""/p" -e "/^$opt /p" "$2"/ignore/*)
										echo $p $s >> "$1".subs
										s=$(echo ${s#$opt})
										[ "$s" ] && { l="$l $s" ; : read -p "subs: [$l ] for $opt" ; }
										:
									} || l="$l $opt"
								done
								echo $p$l | tee -a "$2"/opt/all
							done
						done
						[ "$r" = 0 ] && {
							$S nvs "$1".dlt | sort -n | tee "$1".nvs
							CURLIT_SYNC_DRYRUN=1 CURLIT_TABLE_DRYRUN=1 $S dlt "$1".dlt > "$1".rlist
						}
					;;
					reverse_depend_package|rdp)
						local f="$1".dlt
						local pkg=$($S x_name "$f")
						pkg=$(echo $pkg)
						read -p "---$pkg---"
						local p r
						while : ; do
							read -p '> ' r
							[ "$r" ] || continue
							for p in $($S expac -l ' ' -1 '%N' $r) ; do
								#echo ---$p---
								
								#expr "$pkg" ":" ".* $p " &>/dev/null && echo $p
								setit in pkg $p && echo $p
							done
						done
					;;
					nvs)
						: ${1:?}
						$S expac -1 '%k %n %v %m' $($S x_name "$1")
					;;
					tag_selected_pkg_that_provide) # <tag> <file generated by query_provide>
						: ${1?tag}${2?file}
						local pl=$($S x_name "$1".dlt) p l pro s
						cat "$2" | while read l ; do
						s=
						pro=${l%% *} ; # echo $pro : ${l#$pro}
						for p in ${l#$pro} ; do
							setit in pl $p && s="$s <"$p">" || s="$s $p" 
						done
						echo $pro$s
						done
					;;
					query_seed_pkg) # <tag> <*pkg>
						:
					;;
					install_create_dir|install01)
						[ $# -gt 0 ] && eval $(shit arg $@)
						: ${NEWROOT:?}
						: oblog -t "Check for needed directory"
						local d
						for d in dev/{pts,shm} var/cache/pacman/pkg var/lib/pacman var/log dev run etc etc/pacman.d ; do
							if ! [ -d "$NEWROOT/$d" ]; then
								: oblog -w "Create $NEWROOT/$d directory"
								sudo mkdir -m 0755 -p "$NEWROOT/$d" || return 1
							else
								: oblog "$NEWROOT/$d directory already exist"
							fi
						done
						for d in sys proc ; do
							if ! [ -d "$NEWROOT/$d" ] ; then
								: oblog -w "Create $NEWROOT/$d directory"
								sudo mkdir -m 0555 -p "$NEWROOT"/{sys,proc} || return 1
							else
								: oblog "$NEWROOT/$d directory already exist"
							fi
						done
						if ! [ -d "$NEWROOT/tmp" ]; then
							: oblog -w "Create $NEWROOT/tmp directory"
							sudo mkdir -m 1777 -p "$NEWROOT"/tmp || return 1
						else
							: oblog "$NEWROOT/tmp directory already exist"
						fi
					;;
					install_copy_file|install02)
						[ $# -gt 0 ] && eval $(shit arg $@)
						: ${NEWROOT:?}
						local f 
						if [ ! -e "$NEWROOT/etc/resolv.conf" ]; then
							sudo cp /etc/resolv.conf "$NEWROOT/etc/resolv.conf" || { return 1 ; die " Impossible to copy the file resolv.conf" "clean_install" ; }
						else
							: oblog "File resolv.conf already exists"
						fi
					;;
					install_mount_filesystem|install03)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${CACHEDIR:?}${SYNCDB:? }
						
						fsit mpfs "$NEWROOT" &&
						fsit mb "$CACHEDIR" $NEWROOT/var/cache/pacman/pkg &&
						fsit mb "$SYNCDB" $NEWROOT/var/lib/pacman/sync
					;;
					install_package|install04)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${PACMANCONF:?}${CACHEDIR:?}${PACKAGELIST:?}
						
						local item item_base tidy_loop rc
						local installed list list_base result result_base pacman_list aur_list
						
						if [ ! -e $PACKAGELIST/pkg/base ] ; then
							: die "The file named base at $GENERAL_DIR/$CONFIG_DIR/package_list/ must exist, please create one." "clean_install"
							echo "The file named base at $PACKAGELIST must exist, please create one."
							return 1
						fi
						
						# bash is the first package installed by pacman
						# if $NEWROOT/usr/bin/bash doesn't exist means that is the first pass
						# into the function install_system; so install base package first
						if ! [ -x "$NEWROOT/usr/bin/bash" ] ; then
							read -p "installing base"
							sudo pacman -r "$NEWROOT" -S --config "$PACMANCONF" --cachedir "$CACHEDIR" ${AUTO:+--noconfirm} --dbpath "$NEWROOT/var/lib/pacman" $(grep -h -v ^# $PACKAGELIST/pkg/base) || { return 1 ; die " Failed to install base system" "clean_install" ; }
						fi
						
						installed=$(echo $(pacman -r "$NEWROOT" -Qsq --config "$PACMANCONF"))
						read -p "installed : $installed"
						list_base=$(grep -h -v ^# $PACKAGELIST/pkg/base | sed "/^\s*$/d" | sort -du)
						result_base=$(setit x-y list_base installed)
						read -p "result_base: $result_base"
						
						if [ "$result_base" ]; then
							: oblog -w "Install missing base packages"
							sudo pacman -S -r "$NEWROOT" --config "$PACMANCONF" --cachedir "$CACHEDIR" ${AUTO:+--noconfirm} --dbpath "$NEWROOT/var/lib/pacman" $result_base 2>/dev/null || { return 1 ; die " Failed to install packages" "clean_install" 1 ; }
							result_base=
						else
							: oblog "Nothing to do for base system"
						fi
						
						# check installed packages
						
						: oblog -t "Check installed packages, this may take time..."
						
						installed=$(echo $(pacman -r "$NEWROOT" -Qsq --config "$PACMANCONF"))
						#list=$(grep -h -v ^# $PACKAGELIST/pkg/* $PACKAGELIST/opt/* | sed "/^\s*$/d" | sort -du)
						list=$(for p in $(sed '/^#/d' $PACKAGELIST/pkg/* $PACKAGELIST/opt/*) ; do echo $p ; done | sort -du)
						result=$(setit x-y list installed)
						read -p "result: $(echo $result)"
						
						#install missing package if necessary
						if [ "$result" ] ; then
							! : && {
								for tidy_loop in $result ; do
									choose_installer "$tidy_loop"
									rc=$?
									case "$rc" in
										10)
											unset rc
											pacman_list="$pacman_list $tidy_loop"
										;;
										11)
											unset rc
											aur_list+="$aur_list $tidy_loop"
										;;
										*)
											unset rc
											die "$tidy_loop can not be installed by pacman or AUR helper" "clean_install"
										;;
									esac
								done
							}
							pacman_list=$result
							if [ "$pacman_list" ] ; then
								read -p "pacman_list : $(echo $pacman_list)"
								: oblog -w "Install missing packages coming from repo define in pacman.conf"
								sudo pacman -r "$NEWROOT" -S --config "$PACMANCONF" --cachedir "$CACHEDIR" ${AUTO:+--noconfirm} --dbpath "$NEWROOT/var/lib/pacman" $pacman_list || { return 1 ; die " Failed to install packages with pacman" "clean_install" ; }
							fi
							if [ "$aur_list" ]; then
								read -p "aur_list : $aur_list"
								oblog -w "Install missing packages coming from AUR"
								unset tidy_loop
								for tidy_loop in ${aur_list[@]}; do
									aur_install "$tidy_loop"
								done
							fi
						else
							: oblog "All packages are already installed, nothing to do."
						fi
						
						unset item item_base tidy_loop installed list list_base result result_base pacman_list aur_list
					;;
					install_copy_rootfs|install05)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${ROOTFS:?}
						: oblog -t "Copying configuration files in ${NEWROOT}"
						sudo cp -avf "$ROOTFS"/* "$NEWROOT" || { return 5 ; die " Impossible to copy files" "clean_install" ; }
					;;
					install_set_root_user|install06)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}
						local s=$(sudoit init)
						local pass_exist
						pass_exist=$(sudo grep "root" $NEWROOT/etc/shadow | awk -F':' '{print $2}')
						
						if sudo grep "root::" $NEWROOT/etc/shadow ; then
							: oblog -t "Create root user on $NEWROOT"
							echo "Chg root user shell to /bin/sh"
							sudo usermod -R "$NEWROOT" -s /bin/sh root
						fi
						
						sudo mkdir -p -m 0700 "$NEWROOT/root"
						if [ -z "$pass_exist" -o  "$force" = 1 ]; then
							distroit chroot_passwd "$NEWROOT" "root" toor || { return 6 ; die "unable to set the root password" "clean_install" ; }
						fi
					;;
					install_set_hostname|install07)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${HOSTNAME:?}
						
						if [ "$HOSTNAME" != "" ]; then
							sudo sed -i 's/ .*$//' "$NEWROOT"/etc/hosts
						fi
						
						#echo "$HOSTNAME" > "$NEWROOT"/etc/hostname
						sudo /bin/sh -c "echo $HOSTNAME > $NEWROOT/etc/hostname"
						sudo /bin/sh -c "echo 127.0.0.1	localhost.localdomain	localhost $HOSTNAME > $NEWROOT/etc/hosts"
						sudo /bin/sh -c "echo ::1	localhost.localdomain	localhost $HOSTNAME >> $NEWROOT/etc/hosts"
						
						#sudo sed -i '/127.0.0.1/s/$/ '$HOSTNAME'/' "$NEWROOT"/etc/hosts
						#sudo sed -i '/::1/s/$/ '$HOSTNAME'/' "${NEWROOT}"/etc/hosts
						
						: oblog "hostname was configured successfully"
					;;
					install_set_locale|install08)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}
						local locale
						
						# make sure the variable LOCALE is not empty before launch locale-gen
						locale="${LOCALE:-en_US.UTF-8}"
						sudo sed -i "s:^#${locale}:${locale}:g" "$NEWROOT"/etc/locale.gen
						
						sudo chroot "$NEWROOT" locale-gen
						
						sudo /bin/sh -c "echo LANG=\"$locale\" > \"$NEWROOT\"/etc/locale.conf"
						sudo /bin/sh -c "echo LC_COLLATE=C >> \"$NEWROOT\"/etc/locale.conf"
						
						: oblog "Locale was created successfully"
					;;
					install_set_localetime|install09)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${ZONE:=GMT}
						local LOCALTIME="/usr/share/zoneinfo"
						if [ -n "$SUBZONE" ] ; then
							sudo chroot "$NEWROOT" ln -sf $LOCALTIME/$ZONE/$SUBZONE /etc/localtime
						else
							sudo chroot "$NEWROOT" ln -sf $LOCALTIME/$ZONE /etc/localtime
						fi
						
						: oblog "Localetime was configured successfully"
					;;
					install_set_user|install10)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${NEWUSER:?}
						sudo chroot "$NEWROOT" useradd ${USERID+-u $USERID} -m -G "audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel,video,users" -s /bin/sh "$NEWUSER"
						distroit chroot_passwd "$NEWROOT" "$NEWUSER" toor || { return 6 ; die "unable to set the root password" "clean_install" ; }
						: oblog "User $NEWUSER was created successfully"
					;;
					install_set_xkeymap|install11)
						[ -n "$*" ] && eval $(shit arg $@)
						: ${NEWROOT:?}${XKEYMAP:?}
						if [ -e "$NEWROOT/etc/X11/xorg.conf.d/00-keyboard.conf" ]; then
							: oblog -t "Define keymap for X server in /etc/X11/xorg.conf.d/00-keyboard.conf"
							sudo sed -i 's:Option "XkbLayout"\ .*$:Option "XkbLayout" "'$XKEYMAP'":g' "$NEWROOT"/etc/X11/xorg.conf.d/00-keyboard.conf
							: oblog "Desktop xkeymap was configured successfully"
						fi
					;;
					*)
						echo "$SH setup <dir>"
						echo "$SH gen-pkg > ${R}/pkg.list"
						echo "$SH gen-conf"
						echo "$SH gen-dl <pkg.list> -> pkg.dlt-$(date +%Y%m%d-%H%M)"
						echo "$SH gen-dl-db <dest> > ${R}/curlit.ok"
						echo "$SH gen-dl-hash <dist ...>"
						echo "(CURLIT_SYNC_REMOTE=1 CURLIT_SYNC_INTERACTIVE=1 ${SH} sync-db [dist ...])"
						echo "(CURLIT_SYNC_DRYRUN=1 CURLIT_TABLE_DRYRUN=1 ${SH} dlt <pkg.dlt>)"
						echo "$SH check [dir] [code block]"
						echo "$SH backup <dest> [code block]"
						echo "$SH whatnew <dest> <pkg.list>"
						echo "$SH snapshot <dest> <pkg.list> -> pkg-{snapshot}.xz"
						echo "$SH gen-db <dest> <pkg-{snapshot}.xz>"
						echo "$SH mount_cache <dest>"
					;;
				esac
			}
		;;
		obarunit)
			$self pacmanit                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
			obarunit() {
				local S=obarunit R=${OBARUN_HOME}
				local c=$1
				[ $# -gt 0 ] && shift
				case $c in
					setup)
						: ${1:?}
						PACMAN_REPO_ARCH="core extra community multilib"
						: ${PACMAN_SERVER_ARCH:='http://ftp.iinet.net.au/pub/archlinux/$repo/os/$arch'}
						: ${PACMAN_SERVER_ARCH:='http://mirrors.edge.kernel.org/archlinux/$repo/os/$arch'}
						: ${PACMAN_REPO_OBARUN:="obcore obextra obcommunity observice"}
						: ${PACMAN_SERVER_OBARUN:='https://repo.obarun.org/$repo'}
						PACMAN_DISTS_ORDER="obarun arch"
						: ${PACMAN_HOME:=$(realpath "$1")}
					;;
					mountiso)
						: ${2:?}
						local s=$(realpath "$1")
						local d=$(realpath "$2")
						mkdir -p "$d/iso" &&
						sudo mount -r "$s" "$d/iso" &&
						mkdir -p "$d/sfs" &&
						sudo mount -r "$d/iso/arch/x86_64/airootfs.sfs" "$d/sfs"
						return
					;;
					umountiso)
						sudo umount "$1/sfs" && sudo umount "$1/iso"
						return
					;;
					new-pkgbuild-git)
						: ${1:?}
						local d=$(realpath "$1")
						(
						export GIT_DIR="$d"
						git init --bare $GIT_DIR
						git config repo.url 'https://framagit.org/pkg/$repo/$pkg.git'
						git config repo.refspec '+refs/heads/master:refs/$repo/$pkg/heads/master +refs/tags/*:refs/$repo/$pkg/tags/*'
						)
						return
					;;
					git-fetch-pkgbuild)
						: ${GIT_DIR:?}
						local url=$(git config --get repo.url)
						local refspec=$(git config --get repo.refspec)
						local repo=$1
						shift
						for pkg in $@ ; do
							until
							[ -e pause ] && read -p pause
							echo git fetch -n "$(eval echo $url)" $(eval echo $refspec)
							[ ! -e stop ] && git fetch -n "$(eval echo $url)" $(eval echo $refspec)
							do : ; done
						done
						return
					;;
					xsnap)
						local h d p=${1:-~/pool/homin/ob/snapshot}
						for i in $(cd "$p" ; git log --oneline | awk '{print $1"="$2}') ; do
							h=${i%=*} ; d=${i#*=}
							(cd "$p" ; git archive --prefix $d/ $h) | tar x
						done
					;;
				esac
				local PACMANIT=$S
				pacmanit $c "$@"
			}
		;;
		artixit)
			$self pacmanit
			artixit() {
				local S=artixit R=${PACMAN_HOME}
				local c=$1
				[ $# -gt 0 ] && shift
				case $c in
					setup)
						: ${1:? dir}
						: ${PACMAN_HOME:=$(realpath "$1")}
						: ${PACMAN_REPO_ARCH:="extra community multilib"}
						: ${PACMAN_REPO_ARTIX:="system world galaxy lib32"}
						: ${PACMAN_SERVER_ARCH:='http://ftp.iinet.net.au/pub/archlinux/$repo/os/$arch'}
						: ${PACMAN_SERVER_ARTIX:='http://mirror1.artixlinux.org/repos/$repo/os/$arch'}
						: ${PACMAN_DISTS_ORDER:="artix arch"}
						: ${PACMAN_DB:=$PACMAN_HOME/dist/artix/db}
					;;
					mirror)
						echo "https://gitea.artixlinux.org/packagesP/pacman/raw/branch/master/trunk/pacman.conf"
						echo "https://gitea.artixlinux.org/packagesA/artix-mirrorlist/raw/branch/master/trunk/mirrorlist"
					;;
					summon_obarun)
						: ${PACMAN_REPO_OBARUN:="obextra obcommunity observice"}
						: ${PACMAN_SERVER_OBARUN:='https://repo.obarun.org/$repo'}
						PACMAN_DISTS_ORDER="artix obarun arch"
						return
					;;
					gen_rc_pkg)
						local s
						for s in dinit s6 suite66 ; do artixit expac -s '%n' '.*-'$s | sed 's|-'$s'||' ; done | sort -u
						return
					;;
					gen_rc_list)
						local l=$(sed "/^#/d" "$1") s p
						for s in dinit s6 suite66 ; do
							for p in $l ; do
								echo $p-$s
							done > rc.$s
						done
						return
					;;
				esac
				local PACMANIT=$S
				pacmanit $c "$@"
			}
		;;
		smartfren)
			$self sudoit
			smartfren() {
				local now past pre post p pass
				pass=$(sudoit init 1) || return 1
				while
				#eject /dev/sr0
				#until modprobe -r qcserial ; do read -t 1 ; done
				until lsmod | grep option > /dev/null || { sudoit refresh $pass ; sudo /bin/sh -c "modprobe option && echo '05c6 9201' >> /sys/bus/usb-serial/drivers/option1/new_id" ; } || read -t 1 ; do : ; done
				now=$(date)
				echo $past - $now
				past=${now}
				pre=$(date '+%s')
				sudoit refresh $pass
				PPPD_LINK=001:005:cyborg-e488:smartfren4g:nodetach:3 sudo -E pppd call smartfren4g ttyUSB2 nodetach unit 8
				:
				do
					post=$(date '+%s')
					if [ $(( ${post} - ${pre} )) -lt 1 ] ; then
						read -t 1 p && [ "$p" = p ] && read -p 'slowing pause...'
					fi
					[ -e /run/w/pppd.stop ] && break
					[ -e /run/w/pppd.pause ] && read -t 180 -p 'pppd.pause pause...'
					sudoit refresh $pass
					lsusb ; sudo eject /dev/sr0 ; read -t 1 ; lsusb
				done
			}
		;;
		transmissionit)
			$self setit
			transmissionit() {
				local self=transmissionit
				local c=$1
				shift
				case $c in
					cm)
						: ${1:?}${2:?}${3:?}${4:?}${TRANSMISSIONIT_CMD:?}
						local h=${4:0:16}
						$TRANSMISSIONIT_CMD "$1/$3/"*$h* "$2/$3/"
					;;
					cmt)
						$self cm "$1" "$2" torrents "$3"
					;;
					cmr)
						$self cm "$1" "$2" resume "$3"
					;;
					cp)
						TRANSMISSIONIT_CMD="cp -uv" $self cmt $@ &&
						TRANSMISSIONIT_CMD="cp -uv" $self cmr $@
					;;
					mv)
						TRANSMISSIONIT_CMD="mv -uv" $self cmt $@ &&
						TRANSMISSIONIT_CMD="mv -uv" $self cmr $@
					;;
					rm)
						[ -z "$2" ] && return
						local h=${2:0:16}
						rm "$1/torrents/"*$h* "$1/resume/"*$h*
					;;
					rep_cp)
						local h
						while read -p 'hash ' h ; do [ "$h" ] || continue ; $self cp "$1" "$2" $h ; done
					;;
					search) # dir
						local d m o
						while : ; do
							read -p 'magnet: ' m
							[ -z "$m" ] && continue
							m=${m#magnet:\?xt=urn:btih:}
							m=${m:0:16} ; m=${m,,}
							for d in $@ ; do
								o=$({ ls "$d"/*$m* || ls "$d"/*/*$m* ; } 2>/dev/null)
								[ -n "$o" ] && break
							done
							[ -n "$o" ] && echo "$m found" || echo "$m not found"
						done
					;;
					start)
						: ${1:?}
						[ -f "$1/.gitignore" ] || {
							git init "$1"
							cp "$GITIT_BASE"/heads/tmpl/transmission/{.gitignore,settings.json} "$1"
						}
						c=$(realpath "$1")
						( export TR_CONFIG_DIR="$c" ; transmission-gtk -g "$TR_CONFIG_DIR" &>/dev/null & )
					;;
					w)
						: ${1:?src}${2:?dst}
						local f t
						for f in $1/* ; do
							t="$2/$(date -r "$f" '+%Y%V')"
							[ -d "$t" ] || mkdir -p "$t"
							cp "$f" "$t"
						done
					;;
					q)
						(
						cd "$1"
						while read -p "> " r ; do
							[ "$r" ] || continue
							setit uniq r
							setit pop h r
							s=
							while setit pop i r ; [ "$i" ] ; do
								s="$s | grep -i $i"
							done
							#echo "---$s---"
							if [ "$s" ] ; then eval "grep -i $h *$s"
							else grep -i "$h" *
							fi
						done
						)
					;;
					cpt)
						local s d l
						s=$1
						d=$2
						[ -d "$d" ] || return
						shift 2
						if [ "$1" ] ; then
							for l in $@ ; do
								cp "$s"/torrents/$l.torrent "$d/$(stat -c "%Y" "$s/torrents/$l.torrent")-$l.torrent"
							done
						else
							cp "$s"/torrents/*.torrent "$d"/torrents &&
							cp "$s"/resume/*.resume "$d"/resume
						fi
					;;
					rep_cpt)
						local r
						while read -p '> ' r ; do
							[ "$r" ] && $self cpt "$1" "$2" $r
						done
					;;
					show_title|st)
						transmission-show "$1" | sed -n '/^Name: /s|^Name: ||p'
					;;
					ld|list_dir)
						[ -d "$1/." ] || return
						local f
						for f in "$1"/* ; do $self st "$f" ; done
					;;
					lld)
						[ -d "$1/l" ] || return
						local d n
						for d in $1/?? ; do n=${d##*/} ; $self ld $d/torrents > $1/l/$n ; done
					;;
					*)
						echo "$self cp|mv <srcdir> <destdir> <hash>"
					;;
				esac
			}
		;;
		yt)
			$self curlit
			yt() {
				local S=yt c=$1 H=${YT_HOME:-~/yt}
				shift 2>/dev/null
				case $c in
					update-json|up)
						local o r
						[ $# = 2 ] && o=-country
						until
						#~/youtube-dl -v --cookies cookies --no-check-certificate --write-pages --external-downloader curl --geo-bypass$o $2 -j -f best https://www.youtube.com/watch?v=$1 > "$H/$1"
						#~/youtube-dl -v --cookies cookies --no-check-certificate --write-pages --external-downloader curl -j -f best https://www.youtube.com/watch?v=$1 > "$H/$1"
						#~/youtube-dl -v --no-check-certificate --call-home --external-downloader curl -j -f best --sub-lang en --write-auto-sub https://www.youtube.com/watch?v=$1 > "$H/$1"
						~/youtube-dl -v ${YT_USER:+-u $YT_USER} ${YT_PASS:+-p $YT_PASS} --no-check-certificate --external-downloader curl -j -f best --sub-lang en --write-auto-sub $YT_UP https://www.youtube.com/watch?v=$1 > "$H/$1"
						r=$?
						ls -l -- "$H/$1" 1>&2
						[ -s "$H/$1" ]
						do [ -e yt.stop -o -n "$2" ] && break
						done
						return $r
					;;
					list|ls)
						local LUA_INIT="do local topdir = os.getenv('HOME') package.path = topdir..'/lua/?.lua;' .. package.path end"
						export LUA_INIT
						[ -s "$H/$1" ] || $S up $1
						lua5.3 -l yt -e "print(p('$H/$1',${2:-_}))"
					;;
					list-update|lu)
						local id=${1:?<id>} fmt=${2:-18} URL DIR ALT s r
						until ! [ "$s" = 0 -o -z "$s" ] ; do
							[ -e yt.stop ] && return
							if [ -s "$H/$id" ] ; then
								eval $($S list $id $fmt | sed -n "/^URL=/p")
								s=$(curlit attr "$URL")
								r=$?
								echo $r $s 1>&2
								unset URL
								s=${s%% *}
							fi
							#echo "s=$s" 1>&2
							if [ "$s" = 0 -o -z "$s" ] ; then
								until $S update-json $id ; do [ -e yt.stop ] && break ; read -t 0.3 ; done
							fi
						done
						echo "SIZE=$s;"
						$S list $id $fmt
					;;
					hi)
						curl -kIL "$($S ls $1 $2 | sed -n '/^URL=/s|^URL="\(.*\)";|\1|p')"
					;;
					dl)
						local s r f h
						h=$1 ; shift
						for f in ${@:-18} ; do
							echo $S lu $h $f
							until s=$($S list-update $h $f) ; echo "$s" 1>&2 ; [ -n "$s" ] && eval "$s" ; do
								r=$?
								[ -e yt.stop ] && return
								read -p "return code $r " -t 0.3
								case $r in
									7|10) read -p "return code $r " -t 1
									;;
								esac
								echo
							done
						done
					;;
					lsf)
						: ${2:?}
						local s=$($S ls $1 | sed -n "/^$2 :/p" | sed '/:NA$/d')
						echo $s
						[ -n "$s" ]
					;;
					dls)
						: ${1:?}
						local i ok=0 s
						[ -s "$H/$1" ] || $S up $1
						for i in ${2//,/ } ; do
							s=
							[ -n "$($S ls $1 | sed -n "/^$i :/p")" ] && {
								[ -n "$3" ] && s="$($S ls $1 $i)" || s="$($S lsf $1 $i)"
							}
							[ -z "$s" ] && continue
							$S dl $1 $i && { ok=$i ; break ; }
						done
						[ "$ok" ] && { echo $ok ; return ; }
						return 1
					;;
					dlh)
						local h v
						for h in $@ ; do
							v=$($S dlvi $h)
							[ $? = 0 ] || continue
							case $v in 22|18) continue ;; esac
							$S dlau $h
						done
					;;
					dlo)
						local h=${1:?}
						shift
						: ${@:?}
						local f=$($S min_size $h $@)
						[ -z "$f" ] && return 1
						f=${f% *}
						$S dl $h $f
					;;
					dlvi)
						YT_LOWER_RES=1 $S dl1080p $1
						return
						$S dl1080p $1 ||
						$S dl720p $1 ||
						$S dl480p $1 ||
						$S dl $1 18
					;;
					dlvi-)
						$S dl $1 18 ||
						$S dl480p $1 ||
						$S dl720p $1 ||
						$S dl1080p $1
					;;
					dlau)
						$S dlo $1 251 140 ||
						$S dlo $1 250 249
					;;
					dlpl)
						local f=${1:?} n=${2:-1} h fmt=$3
						local cmd=${YT_CMD:-$S dla}
						while : ; do
							h=$(sed -n "${n}p" "$f")
							[ "---" = "${h:0:3}" ] && { [ "$BREAK" ] && break ; read -p 'pause' ; h=${h#---} ; [ -z "$h" ] && break ; }
							[ -n "$h" -a "${h:0:1}" != "#" ] && 
							if [ -n "$fmt$YT_FMT" ] ; then
								(YT_PLO=$n yt dl $h $([ "$YT_FMT" ] && echo $YT_FMT || echo $fmt))
								unset fmt
							else (YT_PLO=$n $cmd $h)
							fi
							n=$((n+1))
							[ -n "$ONCE" ] && break
							[ "$YT_INTER_DELAY" ] && read -t $YT_INTER_DELAY -p "pause $YT_INTER_DELAY seconds"
						done
					;;
					dlpla)
						local f=$1 n
						shift
						for n in $* ; do
							CURLIT_TEMP_MS=$((1333**3)) BREAK=1 ONCE=${ONCE-1} YT_ALT='' YT_CMD="$S dlau" $S dlpl $f $n
						done
					;;
					dlplv)
						CURLIT_TEMP_MS=$((1333**3)) BREAK=1 xONCE=1 YT_ALT='' YT_CMD=${YT_CMD:-$S dlvi} $S dlpl $1 $2
					;;
					r)
						~/youtube-dl --no-check-certificate --external-downloader curl --external-downloader-args '-b cookies -k' --geo-bypass -v --cookies cookies --write-description --write-info-json --write-annotations --prefer-free-formats -k -o '%(upload_date)s-%(id)s %(title)s-%(format_note)s-%(filesize)s.%(ext)s' -f 18 https://www.youtube.com/watch?v=${1#https://www.youtube.com/watch?v=}
					;;
					sub)
						#~/youtube-dl --sub-lang en --write-auto-sub https://www.youtube.com/watch?v=${1:?}
						#~/youtube-dl -v --call-home --skip-download --sub-lang en --write-sub https://www.youtube.com/watch?v=${1:?}
						~/youtube-dl -v --skip-download --sub-lang ${YT_SUB_LANG:-en} --write-sub https://www.youtube.com/watch?v=${1:?}
					;;
					subf)
						local s
						for s in $(cat "$1") ; do until [ -f *$s* ] ; do yt sub $s ; done ; done
					;;
					subls)
						~/youtube-dl --list-subs https://www.youtube.com/watch?v=${1:?}
					;;
					rdhd) $S rd $1 "$S hd" ;;
					h) sed -i 's|.*/watch?v=\(.\{11\}\).*|\1|' "${1:?}" ;;
					c)
						~/youtube-dl --geo-bypass --geo-bypass-country us --retries infinite --fragment-retries infinite --no-overwrites --continue --write-info-json --verbose --no-check-certificate --sub-lang en --write-sub --keep-video $@
					;;
					dl248)
						local h
						for h in $@ ; do
							until ~/youtube-dl -c -f 248 --fragment-retries infinite --external-downloader curl --external-downloader-args "--connect-timeout 30 -Y 300 -y 19" https://www.youtube.com/watch?v=$h ; do read -t 1 ; done
						done
					;;
					s) while read -p '> ' c ; do [ -n "$c" ] && yt sub $c ; done
					;;
					min_size|ms)
						local LUA_INIT="do local topdir = os.getenv('HOME') package.path = topdir..'/lua/?.lua;' .. package.path end"
						export LUA_INIT
						local f=$1 r
						[ -s "$H/$f" ] || $S up $f
						shift
						r=$(lua5.3 -l yt -e "min_size('$H/$f')" /dev/null "$@")
						[ -n "$r" ] && echo $r || return 1
					;;
					dl480p)
						$S dlo $1 244 135 397 || {
							[ "$YT_LOWER_RES" ] &&
							$S dl360p $1
						}
					;;
					dl720p)
						local v=$($S min_size $1 136 247 398)
						local a=$($S min_size $1 140 251)
						local v22=$($S get_size $1 22)
						local va
						[ "$v" ] && va=$(( ${v#* } + ${a#* } )) || va=$v22
						
						#echo "v: $v"
						#echo "a: $a"
						#echo "v22: $v22"
						#echo "va: $va"
						
						[ "$v22" ] || return 1
						
						[ "$va" -lt "$v22" ] &&
						$S dl $1 ${v% *} ||
						$S dl $1 22
					;;
					dl1080p)
						$S dlo $1 299 399 248 137 || {
							[ "$YT_LOWER_RES" ] &&
							$S dl720p $1
						}
					;;
					dl360p)
						$S dl $1 18
					;;
					get_size)
						: ${1?}${2?}
						echo $(eval $(yt lu $1 $2 2>/dev/null | sed -n '/^SIZE=/p') ; echo $SIZE)
					;;
					dlfmt22)
						$S dl $1 22
					;;
					*)
						echo '(cd ; mkdir yt ; curlit temp . https://yt-dl.org/downloads/latest/youtube-dl ; chmod +x youtube-dl)'
					;;
				esac
			}
		;;
		timeit)
			timeit() {
				local S=timeit
				local c=$1 ; shift 2>/dev/null
				case "$c" in
					epoch)
						date --date "$*" '+%s'
					;;
					at)
						local now=$($S epoch "$($S)")
						local future=$($S epoch "$*")
						#echo $now $future $((future-now))
						read -t $((future-now)) 2>/dev/null
					;;
					pin)
						local now=$(date '+%s')
						local pin=$($S epoch "$*")
						TIMEIT_CORRECTION=$((pin-now))
						echo "TIMEIT_CORRECTION=$TIMEIT_CORRECTION"
					;;
					*)
						local t
						if [ -n "$TIMEIT_CORRECTION" ] ; then
							t=$(date "+%s") ; t="--date @$((t+TIMEIT_CORRECTION))"
						fi
						date $t "${c:-+%Y%m%d %T}"
					;;
				esac
			}
		;;
		pod)
			$self timeit
			pod(){
				local S=pod
				local c=$1
				shift 2>/dev/null
				case $c in
					dl)
						local u=${POD_UNTIL:+$(timeit epoch ${POD_UNTIL})} m f r
						while : ; do
							[ -n "$u" ] && {
								m=$(timeit epoch $(timeit))
								r=$((u-m))
								echo $u $m $r
								[ $r -le 0 ] && break
								m="-m $r"
							}
							local x='-x 127.0.0.1:7777'
							local ua="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Falkon/3.1.0 Chrome/77.0.3865.129 Safari/537.36"
							f=$1-$(timeit "+%Y%m%d-%H:%M:%S").pod
							echo $f
							#echo curl -L -k -N -Y 1 -y 3 $m --connect-timeout 5 "$2" \> "$f"
							curl --http0.9 -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.8' -H 'Upgrade-Insecure-Requests: 1' -A "$ua" -L -k -N -Y 1 -y 3 $m --connect-timeout 5 "$2" > "$f"
							r=$? ; echo $r
							local s=$(stat -c '%s' "$f")
							[ 1000 -gt "$s" ] && { cat "$f" ; rm "$f" ; }
							read -t 0.1
						done
					;;
					dd)
						c=
						[ -n "$4" ] && c="count=$((4*$4))" ; 
						dd if="$1" bs=1024 skip=$((4*$2+${3:-0})) $c > v
					;;
				esac
			}
			alias elshintajkt='pod dl Elshinta "http://202.137.4.147:8000/;"'
			cri2030(){ [ -z "$1" ] && set -- $(timeit) ; (timeit at "$1 20:30:00" ; POD_UNTIL="$1 21:05:00" pod dl Elshinta "http://202.137.4.147:8000/;") ; }
			cri0500(){ [ -z "$1" ] && set -- $(timeit) ; (timeit at "$1 05:00:00" ; POD_UNTIL="$1 05:35:00" pod dl Elshinta "http://202.137.4.147:8000/;") ; }
		;;
		distroit)
			$self fsit sudoit
			distroit(){
				local S=distroit
				local c=$1
				shift 2>/dev/null
				case $c in
					cp)
						: ${1:?<dir>} ${2:?<dir>} ${3:?<expr>}
						local d=$(realpath "$1")
						local D=$(realpath "$2")
						shift 2
						echo "$@"
						(cd "$d" ; tar c $(eval "$@")) | (cd "$D" ; tar x)
					;;
					rm)
						: ${1:?<dir>} ${2:?<expr>}
						local src=$(realpath "$1")
						shift
						#eval "(cd \"$src\" ; rm -rf $@)"
						(cd "$src" ; rm -rf $(eval "$@"))
					;;
					locate)
						: ${1:?<dir>} ${2:?<file>} ${3:?<glob>...}
						local d=$(realpath "$1")
						local f="$2"
						shift 2
						local p s
						for p in $@ ; do
							s=$(cd "$d" ; _f=${f##*/} eval ls "$p" 2>/dev/null) && break
						done && { echo "$s" ; echo "$f" 1>&3 2>/dev/null ; } || echo "$f" 1>&2
					;;
					locate-in)
						: ${1:?<dir>} ${2:?<file>} ${3:?<glob>...}
						local dir="$(realpath "$1")"
						local file="$(realpath "$2")"
						shift 2
						local f
						for f in $(cat "$file") ; do
							$S locate "$dir" "$f" $@
						done
					;;
					whatnew0)
						: ${2:?} : ${3:?<glob>}
						local f l=$(realpath "${2%.dlt}")
						[ -f "$l.dlt.whatnew" ] && mv "$l.dlt.whatnew" "$l.dlt.whatnew~"
						[ -f "$l.dlt.haved" ] && mv "$l.dlt.haved" "$l.dlt.haved~"
						distroit locate-in "$1" "$l.dlt" "$3"/'$_f' 2>"$l.dlt.whatnew" 3>"$l.dlt.haved" >"$l.haved"
						echo "$l.dlt.whatnew"
					;;
					whatnew-c)
						local cache=${1:?dir}
						local fl=${2:?file}
						shift 2
						local pat=$@
						local u p a f match
						for u in $(cat "$fl") ; do
							match=n
							for p in $pat ; do
								a=${p%=*}
								if [ "${u#*$a}" = "$u" ] ; then continue
								else 
									f=${p#*=}/${u##*/}
									[ -f "$cache/$f" ] && echo "$f $u"  ||  echo $u 1>&2
									match=y
									break
								fi
							done
							[ $match = n ] && echo $u 1>&3
						done
					;;
					whatnew)
						local cache=${1:?dir}
						local file=${2:?file}
						local p="$file.$(date '+%H%M')"
						
						$S whatnew-c $@ > "$p.cached" 2>"$p.new" 3>"$p.hmm"
						echo "$p.new"
					;;
					check)
						: ${1:?}
						local d=$(realpath "$1")
						shift
						#echo "$@"
						#rm compress.err 2>/dev/null
						(cd "$d" ; $S check-archive $(eval "$@")) 2>compress.err
						[ ! -s compress.err ] && rm compress.err
					;;
					check-archive)
						local f
						local p=$(pwd)
						for f in $@ ; do
							case $f in
								*.xz|*.txz) xz -tqq "$f" ;;
								*.bz2|*.tbz2) bzip2 -tq "$f" ;;
								*.zst|*.tzs) zstd -tqq "$f" ;;
								*.apk|*.gz|*.tgz) gzip -tq "$f" ;;
								*) ! : ;;
							esac || echo "$p/$f" 1>&2 && echo "$p/$f"
						done
					;;
					gen-db)
						: ${1:?<dir>} ${2:?<glob>}
						local d="$(realpath "$1")"
						(
						cd "$d"
						local f
						for f in $(ls $2) ; do
							set -- $(stat -L -c "%n %s %Y" "$f")
							[ -n "$NOPATH" ] && echo "${1##*/} $2 $3" || echo $@
						done
						)
					;;
					attr)
						local u f s r
						for u in $(cat "$1") ; do
							echo "$u" > __CHECKING__
							f=${u##*/}
							s=$(curlit attr "$u" $(curlit exist-db "$f"))
							r=$?
							set -- $s
							[ $r != 0 ] && echo "$r $3 $u"
							[ -f stop ] && break
						done
					;;
					pkg-x-list)
						: ${1:?pkg.xz}
						sed -e '/^#/d' -e '/^$/q' "$1" | sed '/^$/d'
					;;
					pkg-x-path)
						: ${1:?pkg.xz}
						sed '1,/^$/d' "$1"
					;;
					gen-diff)
						: ${1:?pkg.xz} ${2:?pkg.list} ${3:?pkg.haved}
						local t==$(mktemp -u XXXXX)
						$S pkg-x-path "$1" > "$t.base"
						diff "$t.base" "$3" | grep '^> ' | sed 's|^..||' | sort -u > "$t.new"
						local p=${1##*/}
						p=${p%.xz}
						{ cat "$2" ; echo ; cat "$t.new" ; } | xz -9 > "$p"+$(date '+%Y%m%d').xz
						rm $t.*
					;;
					ol)
						: ${1:?dir}
						local p
						for p in $(sed '/^#/d' "$1"/*) ; do echo $p ; done | sort -u
					;;
					chroot_passwd)
						: ${1:?rootdir}${2:?user}${3:?password}
						local root="$1" user="$2" pass="$3"
						printf "%b\n" "$pass\n$pass" | sudo -n chroot "$root" passwd "$user"
					;;
					chroot)
						: ${1:?dir} user=$3 mfs=$4
						local newroot=$(realpath "$1") overlay="$2" theroot d p user=$3 mfs=$4
						p=$(sudoit init 1) || return 1
						
						theroot="$newroot"
						[ "$overlay" ] && theroot=$(fsit mov "$newroot" "$overlay") || return 1
						
						local FS_MINIMAL=$mfs
						fsit mpfs "$theroot" || return 1
						if [ -n "$BIND_DIRS" ] ; then
							for d in $BIND_DIRS ; do
								fsit mo "$d" "$theroot/$d" -r -o bind
							done
						fi
						clear
						tput cnorm
						echo "Type exit when you have finished"
						if [ "$user" ] ; then
							sudo chroot "$theroot" /usr/bin/env -i HOME=/home/$user TERM="$TERM" PS1='\u:\w\# ' PATH=/bin:/usr/bin:/sbin:/usr/sbin LESSHISTFILE=- HISTFILE=/tmp/.sh_history_"$user" /bin/su -s /bin/sh "$user"
						else
							sudo chroot "$theroot" /usr/bin/env -i HOME=/root TERM="$TERM" PS1='\u:\w\$ ' PATH=/bin:/usr/bin:/sbin:/usr/sbin LESSHISTFILE=- HISTFILE=/tmp/.sh_history_root /bin/sh
						fi
						[ "$AUTO_UMOUNT" ] && {
							sudoit refresh $p
							[ "$overlay" ] && fsit ua "${theroot%/*}"
						}
					;;
				esac
			}
		;;
		zstdit)
			#$self curlit
			zstdit() {
				local S=zstdit c=$1
				shift 2>/dev/null
				case $c in
					cp|cpc)
						local d="$1"
						shift
						local p f
						echo
						for p in "$@" ; do
							[ -f "$p" ] || continue
							f=${p##*/}
							echo -n "$d/$f.zst "
							if [ ! -f "$d/$f.zst" ] ; then 
								if [ "$c" = cp ] ; then
									zstd --rm -19 -q "$p"
									/bin/cp -ud --preserve=all "$p".zst "$d"
								else cat "$p" | zstd -19 > "$d/$f".zst
								fi
								zstd -tq "$d/$f".zst && echo OK || echo NOK
							else echo AEX
							fi
						done
						sync
					;;
					d)
						[ ! -d "$1" ] && return
						local d="$1" f
						until f=${d##*/} ; d=${d%/*} ; [ -n "$f" ] ; do : ; done
						#read -p "$d $f"
						if [ "$d" = "$f" ] ; then tar c "$f"
						else (cd "$d" ; tar c "$f")
						fi | zstd -19 > "$f".tzs
					;;
					check)
						local d f p
						d="${1:?}"
						while read f ; do
							for d in "$@" ; do
								p=${f%.zst} ; p=$p.zst
								[ -f "$d/$p" ] &&
								{ zstd -tq "$d/$p" && echo -n OK || echo -n NOK ; echo " in $d" ; } ||
								echo NA
							done
						done
					;;
					rm)
						local i
						for i in "$@" ; do rm "$i".zst ; done
					;;
				esac
			}
		;;
		objwm)
			objwm() {
				: ${1:?'/run/archiso/img_dev/pkg/Obarun-Plasma_x86_64-2019-05.279b8bc4aa69c16e79e9fd5e696fc45e.pkgs.txz'}
				: ${2:?'/run/archiso/img_dev/pkg/alpine/backup_static-20200607.tzst'}
				sudo -k ; sudo -v
				cd
				mkdir -p z w mnt
				sudo mv z w mnt /run
				ln -s /run/z .
				mount_partition() {
					local d l p
					IFS=$'\n'
					for l in $(lsblk -P -o TYPE,HOTPLUG,FSTYPE,PATH,MOUNTPOINT,UUID) ; do
						unset IFS
						set -- $l
						if [ "$5" = "MOUNTPOINT=\"\"" ] ; then
							case "$3" in
								FSTYPE=\"ext?\")
									d=${4#*=\"} ; d=${d%?}
									p=/run/mnt/${d##*/}
									#echo ${1} ${4} ${3}
									mkdir -p $p ; sudo mount -r $d $p
								;;
							esac
						fi
					done
				}
				mount_partition ; unset mount_partition
				(
					mkdir z/pkg
					cd z/pkg
					tar xfJ "$1"
					sudo pacman -U --noconfirm dist/*/repo/*/{\
gnome-mpv-*,lua52-5.2.*,mpv-*,libcdio-paranoia-*,rubberband-*,vamp-plugin-sdk-*,\
xvkbd-*,libxp-*,xaw3d-*,\
spacefm-*,udevil-*,ffmpegthumbnailer-*,\
lxterminal-*,vte3-*,vte-common-*,\
fbreader-*,libunibreak-*,\
lua51-luajson-*,lua51-lpeg-*,lua51-5.1.*,\
sylpheed-*,compface-*,gtkspell-*,enchant-*,aspell-*,hspell-*,libvoikko-*,\
hdparm-*,\
lxappearance-gtk3-*,\
transmission-gtk-*,\
ttf-droid-*,\
alsa-utils-*,\
squashfs-tools-*,\
jwm-*,\
xtrlock-*,\
hexchat-*\
}
				)
				sudo 66-disable networkmanager ; sudo 66-stop networkmanager
				sudo 66-disable wpa_supplicant ; sudo 66-stop wpa_supplicant
				sudo killall -9 ModemManager
				#local D=$(ls -d /run/mnt/*/obarun-20180915/pacman)
				(cd / ; sudo tar x --zstd -f "$2")
				sudo ln -s ~/.Xdefault /root/.Xresources
				. ~/.Xprofile
				rm -r .cache .local ; mkdir z/.cache z/.local ; ln -s z/.cache z/.local .
				ln -s z/t/transmission-finish
			}
		;;
		bckp)
			$self shit
			bckp() {
				local __S=bckp __c=$1
				shift 2>/dev/null
				case $__c in
					polipo)
						local d="${1:?}"
						d="$(realpath "$1")"
						#read
						local p=$(pwd)
						(
							cd "$d"
							(cd polipo-cache ; ls) | zstd -19 > list.zst
							tar c polipo-cache | zstd -19 > polipo-cache.tzs
							tar c list.zst polipo-cache.tzs > ${2+${2}/}polipo-cache-$(date +%Y%m%d-%H%M).tar
						)
					;;
					static)
						local BACKUP_user=~/.gitconfig

						sudo -k ; sudo -v
						#sudo ls -l $(shit lvpre BACKUP_) | less -S
						sudo tar c $(shit lvpre BACKUP_) | zstd -19 > backup_static-$(date '+%Y%m%d').tzs
					;;
					data)
						( cd ; tar c z/t | zstd -19 > z/z.$(date '+%Y%m%d').tzs )
					;;
					data_add)
						( local t="$(readlink -f "$1")" IFS=$'\n' ; cd ~/z/t ; tar c xs note $(find torrents -type f -newer "$t" -exec /bin/sh -c 't="{}" ; r=resume${t#torrents} ; r=${r%torrent}resume ; echo "$t" ; [ -f "$r" ] && echo "$r"' \;) ) | zstd -19 > "z+$(date +%Y%m%d%H%M).tzs"
					;;
				esac
			}
		;;
		polipo)
			polipo_alpine(){
				: ${APK_HOME:=$(pwd)/distros}
				: ${APK_VERSION_ALPINE=v3.11}

				export APK_HOME APK_VERSION_ALPINE
				: ${1:?ALPINE_TARBALL}${2:?LUAPOLIPO}${3:?XS}
				local ALPINE_TARBALL="$1"
				local LUAPOLIPO="$2"
				local XS="$3"

				#[ ! -e "${ALPINE_TARBALL}" -o ! -e "${LUAPOLIPO}" ] && { echo "not exist" ; return ; }
				sudo -E /bin/sh -c ". $XS alpineit ; alpineit setup $APK_HOME ; (cd $APK_HOME/dist ; tar xf $ALPINE_TARBALL) ; alpineit init alpine-baselayout apk-tools alpine-keys ; mkdir -p \$APK_SYSROOT/run/tmp/cache ; chmod -R o+r,o+w,o+x \$APK_SYSROOT/root \$APK_SYSROOT/run/tmp ; alpineit mountsys ; echo \$APK_SYSROOT > $APK_HOME/path"

				APK_SYSROOT=$(cat $APK_HOME/path)
				sudo chroot "$APK_SYSROOT" /usr/sbin/adduser -D -u 3333 -s /bin/sh hmq
				#sudo chroot "$APK_SYSROOT" /bin/chown -R 3333:3333 /home/hmq
				#sudo chroot "$APK_SYSROOT" /bin/sed -i 's|1001:1001|1000:1000|' /etc/passwd

				( cd "$APK_SYSROOT"/home/hmq ; tar xf ${LUAPOLIPO} )
				sudo cp -f /etc/resolv.conf /etc/hosts "$APK_SYSROOT"/etc

				#apk add --allow-untrusted lua5.1-sec lua5.1-copas lua5.1-curl lua5.1-filesystem lua5.1-inspect lua5.1-md5 lua5.1-basexx lua5.1-lzlib
				sudo chroot "$APK_SYSROOT" /sbin/apk add --allow-untrusted lua5.3-sec lua5.3-copas lua5.3-curl lua5.3-filesystem lua5.3-inspect lua5.3-md5 lua5.3-basexx lua5.3-lzlib
				#apk add --allow-untrusted openssl curl
				sudo -E /bin/sh -c ". $XS alpineit ; alpineit setup $APK_HOME ; alpineit umountsys $APK_SYSROOT"
				unset APK_HOME APK_SYSROOT APK_VERSION_ALPINE
			}

cat <<"#POLIPO#"
polipo_alpine '/a/d/alpine-3.11.polipo.txz' '/a/d/lua-polipo-3.txz' '/run/pool/homin/heads/script/xs'

distroit chroot alpine/sysroot/alpine/v3.11/x86_64 '' hmq 1

export LUA_INIT="do local topdir = os.getenv('HOME') package.path = topdir..'/polipo/?.lua;' .. package.path end"

d="${HOME}/polipo-cache $(ls -dr ${HOME}/polipo-cache-*)";d="$(echo $d)";d=${d// /;};lua5.3 ${HOME}/polipo/xavante-polipo.lua 7777=${d}

lua5.3 -e "Polipo = require 'polipokit' OW='yes' Polipo.params.inter.minimalLength=300000 Polipo.params.inter.minimalLengthCutOff=1 Polipo.repl_cache_alt('${HOME}/polipo-cache')"

lua5.3 -e "xDEF_ACT='overwrite' Polipo = require 'polipokit' OW='yes' Polipo.params.inter.minimalLength=30000 Polipo.params.inter.minimalLengthCutOff=nil Polipo.repl_cache_alt('${HOME}/polipo-cache',1)"
#POLIPO#

		;;
		0)
			$self gitit transmissionit zstdit timeit fsit md5it
			alias cp='cp -uvd --preserve=all'
			alias T="transmissionit start /run/pool/homin/heads/T"
			alias TH='(d=/run/pool/rm/th/$(eval timeit "+%Y/%V") ; [ -d "$d/torrents/.git" ] || git init "$d/torrents" ; transmissionit start "$d")'
			alias kq='killall mpv /usr/bin/kded5 kdeinit5 konqueror kglobalaccel5 /usr/lib/kactivitymanagerd /usr/lib/accounts-daemon /usr/lib/polkit-1/polkitd /usr/lib/upowerd baloo_file gnome-mpv spacefm okular falkon 2>/dev/null'
			alias lsblk='lsblk -o NAME,LABEL,MOUNTPOINT,SIZE,UUID'
			#alias benqhd='xrandr --newmode "1368x768_60.00"   85.25  1368 1440 1576 1784  768 771 781 798 -hsync +vsync ; xrandr --addmode DP1 1368x768_60.00 ; xrandr --output DP1 --mode 1368x768_60.00'
		;;
		3)
			[ -d /run/pool ] || {
				mkdir -p pool/rm/yt w yt
				ln -s /run/pool/homin/heads/script/lua
				touch w/pppd.pause w/pppd.stop-
				sudo mv pool w /run
			}
		;;
		gitlab_common)
			#$self [+++]
			gitlab_common() {
				local __S=gitlab_common __c=$1 s
				local api=${GITLAB_API:-/api/v4}
				local host=${GITLAB_HOST:-https://framagit.org}
				shift 2>/dev/null
				local s q
				case $__c in
					user-query|uq) s="/users?username=$1" ;;
					user-query-id|uqi) s="/users/$1" ;;
					user-project|up) s="/users/$1/projects" ;;
					project-id|pi) s="/projects/$1" ;;
					group-query|gq) s="/groups?search=$1&top_level_only=true" ;;
					group-subgroup|gs) s="/groups/$1/subgroups" ;;
					group-project|gp) s="/groups/$1/projects" ;;
					*)
						echo "$__S uq|user-query <str>"
						echo "$__S ui|user-query-id <num>"
						echo "$__S up|user-project <num>"
						echo "$__S pi|project-id <num>"
						echo "$__S gq|group-query <str>"
						echo "$__S gs|group-subgroup <num>"
						echo "$__S gp|group-project <num>"
						return
					;;
				esac
				q=${s%%\?*}
				[ "$q" = "$s" ] && q='?' || q='&'
				[ -n "$GITLAB_PAGE" ] && s="$s$q""per_page=${GITLAB_PERPAGE:-100}&page=$GITLAB_PAGE"
				echo $host$api$s
				curl $GITLAB_CURL "$host$api$s"
			}
		;;
		pathit)
			pathit(){
				local __S=pathit __c=$1
				shift 2>/dev/null
				case $__c in
					tail)
						local b=$(realpath $1) d=$2 p=$2 q match=0
						while : ; do
							[ "$b" = "$(realpath $p)" ] && { match=1 ; break ; }
							q=${p%/*}
							[ "$q" = "$p" ] && break || p=$q
							#read -p "$q"
						done
						[ "$match" = 1 ] && { q=${d#$p} ; q=${q#?} ; [ -n "$q" ] && echo $($__S tidy $q) ; }
					;;
					tidy)
						echo "$@" | sed -e 's|/\+|/|g' -e 's|/*$||'
					;;
					cat)
						local f p
						for p in $@ ; do f=${p##*/} ; echo "### $f" ; cat $p ; done
					;;
					commentsort)
						local tmp=$(mktemp /tmp/commentsort.XXXXX) dst=$(mktemp /tmp/commentsort.XXXXX) h t
						sed -n '/^##*/p' "$1" > $tmp
						sed -e '/^#*[ \t#]*$/d' -e 's/^##*//' "$1" | sort -u > $dst
						#cat $tmp
						for w in $(cat $tmp) ; do
							h=${w%%[^#]*}
							t=${w##$h}
							t=$(echo $t | sed  's/\//\\\//g')
							#echo $w $h$t
							sed -i "/^$t$""/s/.*/$h$t/" $dst
						done
						rm $1
						cp $dst "$1"
						rm $tmp $dst
					;;
				esac
			}
		;;
		fsit)
			#$self [...]
			fsit(){
				local __S=fsit __c=$1
				shift 2>/dev/null
				case $__c in
					mount_once|mo)
						#arg="$@" sudo -E /bin/sh -c 'f(){ [ -d "$1" ] || mkdir -p "$1" ; local m=$(realpath "$1") ; shift ; if mount | grep " $m " &>/dev/null ; then : ; else mount "$@" ; fi ; } ; f $arg'
						: ${2:?path dir [options...]}
						local p=$(realpath -m "$2")
						mount | awk '{print $3}' | grep "^$p$" && return
						[ -d "$p" ] || mkdir -p "$p" || return 1
						sudo mount "$@" || return 1
						echo "$p"
					;;
					umount_once|uo)
						#arg="$@" sudo -E /bin/sh -c 'f(){ local m=$(realpath "$1") ; if mount | grep " $m " ; then umount "$m" ; fi ; } ; for d in $arg ; do f $d ; done'
						local p r
						for p in "$@" ; do
							r=$(realpath "$p")
							mount | awk '{print $3}' | grep "^$r$" && sudo umount "$r"
						done
					;;
					mount_bind|mb)
						$__S mo "$1" "$2" -o bind
					;;
					mount_map|mm)
						local d="$1" s
						shift
						for s in $@ ; do
							$__S mb "$s" "$d/$s"
						done
					;;
					mount_pseudo_fs|mpfs)
						local d=${1:?dir}
						d=$(realpath "$d") || return 1
						$__S mo proc "$d/proc" -t proc -o nosuid,noexec,nodev &&
						$__S mo sys "$d/sys" -t sysfs -o nosuid,noexec,nodev,ro &&
						$__S mo dev "$d/dev" -t devtmpfs -o mode=0755,nosuid &&
						$__S mo devpts "$d/dev/pts" -t devpts -o mode=0620,gid=5,nosuid,noexec &&
						$__S mo shm "$d/dev/shm" -t tmpfs -o mode=1777,nosuid,nodev &&
						$__S mo tmp "$d/tmp" -t tmpfs -o mode=1777,strictatime,nodev,nosuid &&
						[ "$FS_MINIMAL" ] || $__S mo run "$d/run" -t tmpfs -o nosuid,nodev,mode=0755 &&
						return ||
						$__S ua "$d"
						return 1
					;;
					umount_pseudo_fs|upfs)
						local d=${1:?dir}
						$__S uo "$d/proc" "$d/sys" "$d/dev/pts" "$d/dev/shm" "$d/run" "$d/tmp" "$d/dev"
					;;
					mpf_minimal|mpfm)
						local FS_MINIMAL=1
						$__S mpfs $@
					;;
					upf_minimal|upfm)
						local d=${1:?dir}
						$__S uo "$d/proc" "$d/sys" "$d/dev/pts" "$d/dev/shm" "$d/tmp" "$d/dev"
					;;
					ua|umount_all)
						local p s r
						p=$(realpath "$1")/ || return
						[ "$p" = '/' ] && return
						p="/^${p//\//\\\/}/"
						while : ; do
							s=$(mount | awk '$3 ~ '"$p"' {print $3}')
							#echo ; read -p "$s >"
							[ "$s" -a "$s" != "$r" ] && $__S uo $s || break
							r=$s
						done
						! [ "$s" ]
					;;
					mount_overlay|mov)
						: ${1?dir}${2?dir}
						local root over
						root=$(realpath "$1")
						over=$(realpath "$2")
						mkdir -p "$over"/{r,u,w} &&
						$__S mo overlay "$over"/r -t overlay -o lowerdir="$root",upperdir="$over"/u,workdir="$over"/w &&
						echo "$over"/r
					;;
					cpn|cp_if_newer)
						: ${1:?src}${2:?ref}${3:?dst}
						find "$1" -newer "$2" -type f -exec cp -vud {} "$3" \;
					;;
				esac
			}
		;;
		sudoit)
			#$self [+++]
			sudoit() {
				local __S=sudoit __c=$1
				shift 2>/dev/null
				case $__c in
					init)
						local s
						[ -n "$*" ] && sudo -K || { sudo -n true 2>/dev/null && return ; }
						read -s -p "paassword(sudo): " s
			 			$__S refresh $s || return 1
						echo $s
					;;
					refresh)
						sudo -K
						#echo $1 | sudo -Sv -p ''
						echo $1 | sudo -Sv 2>/dev/null
					;;
				esac
			}
		;;
		lfsml)
			#$self [+++]
			lfsml() {
				local __S=... __c=$1
				shift 2>/dev/null
				case $__c in
					gen)
						: ${1:?dir}
						local p q
						for p in $(find "$1" -type f ! -regex '.*\.git.*') ; do
							q=${p#"$1"/}
							echo "$2${2:+/}$q"
						done
					;;
					dl)
						local l d
						local p=${2:-"https://www.linuxfromscratch.org/~thomas/multilib"}
						for l in $(cat "$1") ; do
							d=${l%/*}
							[ "$l" = "$d" ] || mkdir -p "$d"
							curl -k -x 127.0.0.1:7777 "$p/$l" > "$l"
						done
					;;
				esac
			}
		;;
		github)
			$self gitit
			github() {
				local __S=github __c=$1
				shift 2>/dev/null
				case $__c in
					cb)
						local u=${1:?url}
						u=${u%.git}
						u=${u#https://github.com/}
						shift
						git init --bare $u.git
						(GIT_DIR=$u.git gitit fetch_depth $1 ${@:-master})
					;;
					combine)
						: ${1:?repo} ${2:?dir}
						local d r=$1 p=$2 n
						for d in $p/*/* ; do
							n=${d#$p/}
							n=${n%.git}
							(GIT_DIR=$d gitit push_namespace $r $n)
						done
					;;
					fh|fetch_head)
						local d=$1
						shift
						for r in $@ ; do GIT_DIR=$d GITIT_NAMESPACE=$r gitit fetch_init https://github.com/$r master ; done
					;;
				esac
			}
		;;
		setit)
			$self shit
			setit() {
				local __S=setit __c=$1
				shift 2>/dev/null
				local __ws=$(printf " \t\na") ; __ws=${__ws%?}
				local __nl=${__ws#??}
				case $__c in
				a-b)
					local i j found
					local a=$(shit ive $1)
					local b=$(shit ive $2)
					[ "$SORTED" ] || $__S uniq b
					for i in $a ; do
						found=n
						for j in $b ; do
							[ "$i" = "$j" ] &&
							{ found=y ; break ; } || 
							[ "$i" '<' "$j" ] &&
							break
						done
						[ "$found" = n ] && echo $i
					done
					:
				;;
				a.b)
					local i j
					local a=$(shit ive $1)
					local b=$(shit ive $2)
					[ "$SORTED" ] || {
						$__S uniq a
						$__S uniq b
					}
					i=$(echo $a | wc -w)
					j=$(echo $b | wc -w)
					#echo a: $i b: $j
					if [ $i -gt $j ]; then
						j=$a ; a=$b ; b=$j
					fi
					for i in $a ; do
						for j in $b ; do
							#echo i:$i ; echo j:$j ; read -p '>'
							if [ "$i" = "$j" ] ; then
								echo $i
								[ "$ONCE" ] && return
								break
							elif [ "$i" '<' "$j" ] ; then break
							fi
						done
					done
					:
				;;
				in)
					local e=$2 i
					local a=$(shit ive $1)
					[ "$SORTED" ] || $__S uniq a
					for i in $a ; do [ "$i" = "$e" ] && return || [ "$i" ">" "$e" ] && break ; done
					return 1
				;;
				fa-fb)
					: ${1?}${2?}
					local a=$(cat "$1")
					local b=$(cat "$2")
					$__S a-b a b
				;;
				fa.fb)
					: ${1?}${2?}
					local a=$(cat "$1")
					local b=$(cat "$2")
					$__S a.b a b
				;;
				pop)
					eval $1'=${'$2'%%[$__ws]*}'
					eval "$2="'${'$2'#*[$__ws]}'
				;;
				push)
					eval 'if [ ${#'$2'} = 0 ] ; then '$2'=$'$1' ; else '$2'=${'$2'}${__nl}${'$1'}$__nl ; fi'
				;;
				uniq)
					eval "$1"'=$(for i in $'"$1"' ; do echo $i ; done | LC_ALL=C sort -u)$__nl'
				;;
				line)
					local i
					for i in $(shit ive $1) ; do echo $i ; done
				;;
				diff)
					local X=$(shit ive $1)
					local Y=$(shit ive $2)
					local x y actx acty actc
					case "$3" in
						'<') actx='echo $x' ;;
						'>') acty='echo $y' ;;
						'=') actc='echo $x' ;;
						*)
							actx='echo "<$x"'
							acty='echo ">$y"'
							actc='echo "=$x"'
						;;
					esac
					if [ "$ONCE" ] ; then
						local s
						for s in actx acty actc ; do
							[ "$(shit ive $s)" ] && eval "$s"'="$'$s' ; return"'
						done
					fi
					[ "$SORTED" ] || {
						$__S uniq X
						$__S uniq Y
					}
					$__S pop x X
					$__S pop y Y
					while [ "$x$y" ] ; do
						if [ ! "$x" ] ; then
							if [ ! "$acty" ] && { [ "$actx" ] || [ "$actc" ] ; } ; then break ; fi
							while [ "$y" ] ; do eval "$acty" ; $__S pop y Y ; done
						elif [ ! "$y" ] ; then
							if [ ! "$actx" ] && { [ "$acty" ] || [ "$actc" ] ; } ; then break ; fi
							while [ "$x" ] ; do eval "$actx" ; $__S pop x X ; done
						else
							if [ "$x" '<' "$y" ] ; then
								eval "$actx"
								$__S pop x X
							elif [ "$x" '>' "$y" ] ; then
								eval "$acty"
								#echo "$y" 1>&2
								$__S pop y Y
							else
								eval "$actc"
								#echo "$x" 1>&3
								$__S pop x X
								$__S pop y Y
							fi
						fi
						#read -p "${#x}:${#X} ${#y}:${#Y}"
					done
				;;
				x-y)
					$__S diff $1 $2 '<'
				;;
				x.y)
					$__S diff $1 $2 '='
				;;
				lua_fa-fb)
					: ${1?}${2?}
					lua5.3 -e 'b = "\n"..io.open(arg[2]):read "*a" fa = io.open(arg[1]) repeat a = fa:read "*l" if not a then break end if not b:match("\n"..a:gsub("%-","%%-"):gsub("%.","%%."):gsub("%+","%%+").."\n") then print(a) end until nil fa:close()' /dev/null "$1" "$2"
				;;
				esac
			}
		;;
		shit)
			shit(){
				local __s=shit
				local __c=$1
				shift 2>/dev/null
				
				case $__c in
					norm)
						local n=$1
						n=${n//[+.-]/_}
						echo $n
					;;
					lv|list_variables)# ${!var*}
						set | sed -n "/^$1/s|=.*||p"
					;;
					ive|indirect_variable_expansion) # ${!var}
						[ "$1" ] && echo $(eval echo "$"$1)
					;;
					lvpre)
						local i
						for i in $($__s lv) ; do [ "${i#$1}" = "$i" ] || echo $i ; done
					;;
					arg)
						local r k
						while [ -n "$1" ] ; do
							[ "${1#*=}" = "$1" ] && r="$r $1" || k="$k $1"
							shift
						done
						[ -n "$k$r" ] && echo local$k$r || echo :
					;;
					pipe_or_arg)
						: ${SHIT_CMD:?}
						local c _r_
						[ "$1" ] && c='_r_=$1 ; shift 2>/dev/null' || c='read _r_'
						while eval "$c" ; do
							eval "$SHIT_CMD"
						done
					;;
					packn)
						local n
					;;
					
				esac
			}
		;;
		md5it)
			md5it() {
				local __S=md5it __c=$1
				shift 2>/dev/null
				case $__c in
					h|hash) md5sum "$1" | sed 's| .*$||' ;;
					e|embed)
						local f h e
						for f in "$@" ; do
							h=$($__S h "$f")
							e=${f##*.}
							[ -e *.$h.$e ] || ln "$f" "${f%.*}.$h.$e"
						done
					;;
					x)
						local h
						h=${1%.*}
						h=${h##*.}
						echo $h
					;;
					c|check)
						local f h x
						for f in "$@" ; do
							h=$($__S h "$f")
							x=$($__S x "$f")
							[ "$h" = "$x" ] || echo "$f"
						done
					;;
				esac
			}
		
		;;
		strit)
			#$self [+++]
			strit() {
				local __S=strit __c=$1
				shift 2>/dev/null
				case $__c in
				upper)
					echo $@ | tr '[:lower:]' '[:upper:]'
				;;
				lower)
					echo $@ | tr '[:upper:]' '[:lower:]'
				;;
				esac
			}
		;;
		mpvit)
			$self [+++]
			mpvit() {
				local __S=mpvit __c=$1
				shift 2>/dev/null
				case $__c in
					play)
						local f=$1 t=$2 n=$3
						set -- $(sed -n ${n}p "$t")
						echo Track $n of $f
						mpv --volume=${VOLUME:-50} --start=$1 --length=$2 "$f"
					;;
					shuffle)
						local f=${1:?file} t=${2:?tt} n
						shift 2
						for n in $(shuf -e $@) ; do
							$__S play "$f" "$t" $n
						done
					;;
				esac
			}
		;;
	esac
	[ $# -gt 1 ] && { shift ; $self $@ ; }
}

__def__ $@
unset __def__
return

		...)
			$self [+++]
			...() {
				local __S=... __c=$1
				shift 2>/dev/null
				case $__c in

				esac
			}
		;;

skarnet(){
local mirror='https://github.com/skarnet/$repo.git'
#local mirror='git://git.skarnet.org/skalibs/$repo'
gitit cbd skarnet $m skalibs execline s6 s6-rc s6-portable-utils s6-linux-utils s6-dns s6-networking s6-linux-init lh-bootstrap mdevd utmps nsss bcnm s6-frontend skabus
}

alpine(){
local m='git://git.alpinelinux.org/$repo'
local m='https://git.alpinelinux.org/$repo'
gitit cbd alpine $m abuild alpine-baselayout alpine-conf apk-tools aports ca-certificates docker-abuild lua-aports mkinitfs
}

exherbo(){
local m
m='git://git.exherbo.org/$repo.git'
gitit cbd exherbo $m arbor
m='git://git.exherbo.org/paludis/$repo.git'
gitit cbd exherbo $m paludis paludis-scripts
}

transmission(){
gitit cbd transmission 'https://github.com/transmission/$repo' transmission dht libb64 libevent libnatpmp libutp miniupnpc
}

obarun(){
gitit cbd obarun 'https://framagit.org/Obarun/$repo.git' \
pacman cower rootfs-obarun oblog ConsoleKit2 \
oblibs boot-user-66mod 66 66-tools \
obarun-mkiso obarun-libs obarun-install-themes obarun-install obarun-docker obarun-build \
applysys obarun-lxdm-themes
}

archlinux(){
gitit cbd obarun 'git://git.archlinux.org/$repo.git' \
arch-install-scripts archiso devtools kde-build linux mkinitcpio namcap \
pacman-contrib pacman 
#svntogit/community svntogit/packages
}

nat() {
 local c=$1
 shift
 case $c in
 iface)
  ip addr add ${2}/24 dev ${1}
  ip link set up dev ${1}
  ;;
 list|ls) iptables -nvL ;;
 up)
  iptables --flush ; iptables --table nat --flush ; iptables --delete-chain ; iptables --table nat --delete-chain
  iptables -t nat -A POSTROUTING -o $2 -j MASQUERADE
  iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
  iptables -A FORWARD -i ${1} -o ${2} -j ACCEPT
# iptables -I INPUT -p udp --dport 53 -s 192.168.123.0/24 -j ACCEPT
# iptables -I INPUT -p tcp --dport 53 -s 192.168.123.0/24 -j ACCEPT
  echo 1 > /proc/sys/net/ipv4/ip_forward
  ;;
 *)
  echo "nat iface <dev> <addr/24>"
  echo nat ls
  echo "nat up <local ifece> <internet ifece>"
 esac
}

nat iface eth0 192.168.123.100
nat up eth0 ppp8


# asus

net() {
 ip addr add 192.168.123.1/24 dev enp2s0
 ip route add default dev enp2s0 via 192.168.123.100
 ip link set dev enp2s0 up
}


### \/ mirror
http://www.mirrorservice.org/sites/ [ openmandriva netbsd gutenberg libreboot gentoo-distfiles sourceware ]
http://mirror.aarnet.edu.au/pub/ [ gutenberg netbsd sourceware voidlinux ]
http://mirrors.dotsrc.org/ [ archlinuxarm artix devuan exherbo mxlinux qubes voidlinux ]
http://mirrors.nju.edu.cn/ [ netbsd archlinuxarm lede lfs openresty ]
http://mirror.csclub.uwaterloo.ca/ [ gentoo-distfiles gutenberg netbsd ]
http://mirror.clarkson.edu/ [ artix slitaz voidlinux ]
http://ftp.acc.umu.se/mirror/ [ calculate-linux mxlinux netbsd openmandriva qubes sabayon voidlinux ]
http://ftp.halifax.rwth-aachen.de/ [ archlinuxarm mxlinux qubes slackwarearm ]

http://sourceforge.mirrorservice.org/${project:0:1}/${project:0:2}/${project}/

### /\ mirrorr

### \/ hyperbola
HYPERBOLA_REMOTE_MIRROR='https://ftp.cixug.es/hyperbola/gnu-plus-linux-libre/stable/${repo}/os/${arch}'
HYPERBOLA_REPOS='core extra community multilib'
### /\ hyperbola

skiporabort() {
 local r t
 [[ -z "${1}" ]] &&  t=3 || t=${1}
 read -t ${t} -p "[s]kip [a]bort [p]ause " r
 if [ "$r" = s ] ; then echo 'break'
 elif [ "$r" = a ] ; then echo return
 elif [ "$r" = p ] ; then read -p 'pause... ' r ; echo ':'
 else echo ':'
 fi
}

. /home/hacxzuan/esys3/bash/core.bash 
gppp3 server
while until { lsmod | grep option > /dev/null && modprobe -r option ; modprobe option ; } || read -t 1 ; do : ; done ; echo '05c6 9201' > /sys/bus/usb-serial/drivers/option1/new_id ; now=$(date) ; echo ${past} - ${now} ; past=${now} ; PPPD_LINK=001:005:cyborg-e488:smartfren4g:nodetach:3 pppd call smartfren4g ttyUSB2 nodetach unit 8 ; : ; do : ; done
nat() {  local c=$1;  shift;  case $c in  iface)   ip addr add ${2}/24 dev ${1};   ip link set up dev ${1};   ;;  list|ls) iptables -nvL ;;  up)   iptables --flush ; iptables --table nat --flush ; iptables --delete-chain ; iptables --table nat --delete-chain;   iptables -t nat -A POSTROUTING -o ${2} -j MASQUERADE;   iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT;   iptables -A FORWARD -i ${1} -o ${2} -j ACCEPT;   echo 1 > /proc/sys/net/ipv4/ip_forward;   ;;  *)   echo "nat iface <dev> <addr/24>";   echo nat ls;   echo "nat up <local ifece> <internet ifece>";  esac; }
nat iface eth0 192.168.123.100
nat up eth0 ppp8
